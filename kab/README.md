# README #

Adresář s úkoly do předmětu Kryptografie a bezpečnost.

### 01 work ###

1. Afinní šifra
2. 


### Náležitosti závěrečné zprávy ###

1. OT tak, jak vyšel přímo po dešifrování tj. bez mezer.
2. OT text doplněný o mezery mezi slovy
3. Pokud jste schopni zrekonstruovat i interpunkci, můžete ji doplnit.
4. Přesnou identifikaci použité šifry nebo použité kombinace šifer
5. Používejte terminologii probíranou na přednáškách/cvičeních.
6. Přesnou identifikace šifrovacího klíče:
  * u afinnní šifry - hodnoty koeficientů A,B použité při šifrování
  * u monoalfabetické šifry s heslem - heslo použité k zašifrování
  * u monoalfabetické šifry s posunem - koeficient A použitý při šifrování
  * u Vigenérovy šifry - heslo použité při šifrování
  * u úplné tabulky bez hesla - rozměry tabulky použité při šifrování
  * u úplné tabulky s heslem - rozměry tabulky + číselné pořadí sloupců
  * u dvojnásobné tabulky - rozměry obou tabulek použité při šifrování 
5. Stručný popis, jak jste postupovali při dešifrování