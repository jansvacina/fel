//import java.util.*;
//
///**
// * Created by jan on 25-Oct-17.
// */
//public class Main {
//    public static void main(String[] args) {
//        Main main = new Main();
//        String expectedCipherText = "AGGGF GXGDA AGGAX XAXXV DAAGD DGVAV FDXGA GDXDD GXGGG FDGGA GDFFF FDGXF AXAFF XXAGF AXDAV FAVGA XGAVG FXVGA XGGFA DAFGA AXXVF DAGVF AVXAD GDFGG AAVFD AAADG GGAXA DVA";
//        char[] adfgvx = {'A', 'D', 'F', 'G', 'V', 'X'};
//        String givenAlphabetAndKey = "EDM79FPBSQYROX58JIGN1ACK02LHU4W3VT6Z**QQQ**radio";
//        String plainText = "Educationiswhatremainsafteronehasforgotteneverythinghelearnedinschool";
//        String[] splits = givenAlphabetAndKey.split("\\*");
//        String alphabet = splits[0];
//        String key = splits[4];
//        /**
//         * Create matrix - split alphabet by 6
//         */
//        System.out.println("------------- CREATE MATRIX -------------");
//
//        ArrayList<String> matrix = main.createMatrix(alphabet);
//
//        for (String s : matrix
//                ) {
//            System.out.println(s);
//        }
//        /**
//         * Encrypt plain text
//         */
//        System.out.println("------------- ENCRYPT PLAIN TEXT TO CIPHER TEXT -------------");
//        String cipherText = main.encryptPlainTextToCypherText(matrix, plainText, adfgvx);
//        System.out.println(cipherText);
//        /**
//         * Remove duplicates from key
//         */
//        System.out.println("------------- REMOVE DUPLICATES FROM KEY -------------");
//        String keyWithoutDuplicates = main.removeDuplicatesFromKey(key);
//        System.out.println(keyWithoutDuplicates);
//        /**
//         * Create array to transmit cipher text in
//         */
//        System.out.println("------------- CREATE ARRAY TO TRANSMIT CIPHER TEXT IN -------------");
//        String[] columnsWithCipherText = main.createColumnsWithCipherText(keyWithoutDuplicates, cipherText);
//        for (String s : columnsWithCipherText
//                ) {
//            System.out.println(s);
//        }
//        /**
//         * Sort key without duplicates, while sorting, sort unordered columns as well
//         */
//        System.out.println("------------- SORT KEY and COLUMNS -------------");
//        char[] keyWithoutDuplicatesSortedCharArray = keyWithoutDuplicates.toCharArray();
//        for (int i = 0; i < keyWithoutDuplicatesSortedCharArray.length - 1; i++) {
//            int j = i + 1;
//            char tmpChar = keyWithoutDuplicatesSortedCharArray[j];
//            String tmpString = columnsWithCipherText[j];
//            while (j > 0 && tmpChar < keyWithoutDuplicatesSortedCharArray[j - 1]) {
//                keyWithoutDuplicatesSortedCharArray[j] = keyWithoutDuplicatesSortedCharArray[j - 1];
//                columnsWithCipherText[j] = columnsWithCipherText[j - 1];
//                j--;
//            }
//            keyWithoutDuplicatesSortedCharArray[j] = tmpChar;
//            columnsWithCipherText[j] = tmpString;
//        }
//        System.out.println("- KEY -");
//        System.out.println(keyWithoutDuplicatesSortedCharArray);
//        System.out.println("- COLUMNS - ");
//        for (String s : columnsWithCipherText
//                ) {
//            System.out.println(s);
//        }
//        /**
//         * Merge all char arrays from sorted string array
//         */
//        System.out.println("------------- MERGE COLUMNS -------------");
//        cipherText = main.mergeColumns(columnsWithCipherText);
//        System.out.println(cipherText);
//        /**
//         * Divide cipher text by 5
//         */
//        System.out.println("------------- INSERT SPACES TO CIPHER TEXT -------------");
//        String cipherTextWithSpaces = main.insertSpacesToCipherTextByFive(cipherText);
//
//        System.out.println(cipherTextWithSpaces);
//        System.out.println("------------- TESTING VALIDITY OF CIPHER TEXT -------------");
//        if (cipherTextWithSpaces.equals(expectedCipherText)) System.out.println("!!! ENCRYPTION SUCCESSFUL !!!");
//        else System.out.println("!!! ENCRYPTION FAILED !!!");
//
//        main.decrypt(main, "radio", cipherTextWithSpaces);
//    }
//
//    private void decrypt(Main main, String key, String cipherText){
//        System.out.println("------------- DECRYPTION -------------");
//        String keyWithoutDuplicates = main.removeDuplicatesFromKey(key);
//        System.out.println("------------- REMOVE WHITE SPACES FROM CIPHER TEXT -------------");
//        cipherText = main.removeWhiteSpaces(cipherText);
//        System.out.println("------------- CIPHER TEXT -------------");
//        System.out.println(cipherText);
//        System.out.println("------------- BROKE TO COLUMNS BY LENGTH OF KEY -------------");
//        //transpositionCryptedText
//        String[] columnsWithCipherText = main.createColumnsWithCipherText(keyWithoutDuplicates, cipherText);
//        System.out.println("------------- COLUMNS WITH CIPHER TEXT -------------");
//        for (String s: columnsWithCipherText
//             ) {
//            System.out.println(s);
//        }
//        String sortedKey = main.sortKeyToAlphabeticOrder(key);
//
//        int [] indexesSortedKey = main.indexesSortedKey(key, sortedKey);
//
//        for (int i : indexesSortedKey
//                ) {
//            System.out.println(i);
//        }
//
//        System.out.println("-------------COLUMNAR TRANSPOSITION------------------");
//        String ctEncrypted = "ADDDFDDAXFXAGGFDXXAXFGXFGG";
//        String keyWord = "zebras";
//        System.out.println("Encrypted string " + ctEncrypted);
//        System.out.println("Keyword " + keyWord);
//        String sortedKeyWord = main.sortKeyToAlphabeticOrder(keyWord);
//        System.out.println("Sorted keyword " + sortedKeyWord);
//        int [] indexesSortedKeyWord = main.indexesSortedKey(keyWord, sortedKeyWord);
//        for (int i : indexesSortedKeyWord
//                ) {
//            System.out.println(i);
//        }
//        System.out.println("Indexes length " + indexesSortedKeyWord.length);
//        System.out.println("Cypher text length " + ctEncrypted.length());
//        //how many columns will be longer
//        int[] longerColumnsIndexes = main.getLongerColumnsIndexes(indexesSortedKeyWord, indexesSortedKeyWord.length, ctEncrypted.length());
//        String[] transpositionMatrix = main.createTranspositionMatrix(indexesSortedKeyWord, longerColumnsIndexes, sortedKeyWord, ctEncrypted, main);
//
//        // prochazej indexesSortedKey a z columns vem vzdy string na danem indexu a zmerguj ho do stringu
//
//        //ten zmergovany string pak vem a po dvou pismenech z nej vycitej z matrixu vzdy vycti pismeno
//    }
//
//    private ArrayList<String> createMatrix(String alphabet) {
//        int index = 0;
//        ArrayList<String> matrix = new ArrayList<>();
//        while (index < alphabet.length()) {
//            matrix.add(alphabet.substring(index, index + 6));
//            index += 6;
//        }
//        return matrix;
//    }
//
//    private String encryptPlainTextToCypherText(ArrayList<String> matrix, String plainText, char[] adfgvx) {
//        String cipherText = "";
//        for (char c : plainText.toUpperCase().toCharArray()
//                ) {
//            for (int yIndex = 0; yIndex < matrix.size(); yIndex++) {
//                int xIndex = matrix.get(yIndex).indexOf(c);
//                if (xIndex != -1) {
//                    cipherText += adfgvx[yIndex];
//                    cipherText += adfgvx[xIndex];
//                }
//            }
//        }
//        return cipherText;
//    }
//
//    String removeDuplicatesFromKey(String key) {
//        char[] charKey = key.toCharArray();
//        String keyWithoutDuplicates = "";
//        for (int i = 0; i < charKey.length; i++) {
//            if (keyWithoutDuplicates.indexOf(charKey[i]) == -1)
//                keyWithoutDuplicates += charKey[i];
//        }
//        return keyWithoutDuplicates;
//    }
//
//    String[] createColumnsWithCipherText(String keyWithoutDuplicates, String cipherText) {
//        String[] columnsWithCipherText = new String[keyWithoutDuplicates.length()];
//        for (int i = 0; i < columnsWithCipherText.length; i++
//                ) {
//            columnsWithCipherText[i] = "";
//        }
//        /**
//         * Each sixth to stringArray
//         */
//        int index = 0;
//        String string = "";
//        for (char c : cipherText.toCharArray()
//                ) {
//            string = columnsWithCipherText[index];
//            string += c;
//            columnsWithCipherText[index] = string;
//            index++;
//            if (keyWithoutDuplicates.toCharArray().length == index) {
//                index = 0;
//
//            }
//        }
//        return columnsWithCipherText;
//    }
//
//
//    //
//
//    String mergeColumns(String[] columnsWithCipherText ){
//        String cipherText = "";
//        for (String s : columnsWithCipherText
//                ) {
//            cipherText += s;
//        }
//
//        return cipherText;
//    }
//
//    String insertSpacesToCipherTextByFive(String cipherText){
//        String cipherTextWithSpaces = "";
//        int index = 1;
//        for (char c : cipherText.toCharArray()
//                ) {
//            cipherTextWithSpaces += c;
//            if (index % 5 == 0) {
//                cipherTextWithSpaces += " ";
//            }
//            index++;
//        }
//        cipherTextWithSpaces.trim();
//        return cipherTextWithSpaces;
//    }
//
//    private String sortKeyToAlphabeticOrder(String key){
//        char[] keyCharArray = key.toCharArray();
//        for (int i = 0; i < keyCharArray.length - 1; i++) {
//            int j = i + 1;
//            char tmpChar = keyCharArray[j];
//            while (j > 0 && tmpChar < keyCharArray[j - 1]) {
//                keyCharArray[j] = keyCharArray[j - 1];
//                j--;
//            }
//            keyCharArray[j] = tmpChar;
//        }
//        key = "";
//        for (char c: keyCharArray
//             ) {
//            key += c;
//        }
//        return key;
//    }
//
//    String removeWhiteSpaces(String key){
//        return key.replaceAll("\\s+", "");
//    }
//
////    String [] transpositionCryptedText() {
////
////        return new String[];
////    }
//
//    int[] indexesSortedKey(String key, String sortedKey) {
//        int[] indexesSortedKey = new int[sortedKey.length()];
//
//        for (int i = 0; i < key.length(); i++
//                ) {
//            char k = key.toCharArray()[i];
//            int j = sortedKey.indexOf(k);
//            indexesSortedKey[i] = j;
//        }
//        return indexesSortedKey;
//    }
//
//
//    int[] getLongerColumnsIndexes(int[] indexes, int sortedKeyLength, int cypherTextLength) {
//
//        int countIndexes = cypherTextLength / sortedKeyLength;
//        countIndexes = cypherTextLength - (countIndexes * sortedKeyLength);
//
//        int[] longerColumnsIndexes = new int[countIndexes];
//
//        for (int i = 0; i < countIndexes; i++ ) {
//            longerColumnsIndexes[i] = indexes[i];
//        }
//
//        return longerColumnsIndexes;
//    }
//
//    int getLongerColumnHeight(int sortedKeyLength, int cypherTextLength) {
//        return (cypherTextLength / sortedKeyLength) + 1;
//    }
//
//    int getShorterColumnHeight(int sortedKeyLength, int cypherTextLength) {
//        return cypherTextLength / sortedKeyLength;
//    }
//
//    HashMap<Integer, Integer> createIndexesHashMap(int[] indexesSortedKeyWord) {
//        HashMap<Integer, Integer> hashMap = new HashMap<>();
//        for (int i = 0; i < indexesSortedKeyWord.length; i++) {
//            hashMap.put(indexesSortedKeyWord[i], i);
//        }
//        return hashMap;
//    }
//
//
//    String[] createTranspositionMatrix(int[] indexesSortedKeyWord, int[] longerColumnsIndexes, String sortedKeyWord, String cypherText, Main main) {
//        String[] transpositionMatrix = new String[indexesSortedKeyWord.length];
//        for (String s: transpositionMatrix) { s = "";}
//        int shorterColumnHeight = main.getShorterColumnHeight(sortedKeyWord.length(), cypherText.length());
//        int longerColumnHeight = main.getLongerColumnHeight(sortedKeyWord.length(), cypherText.length());
//        HashMap<Integer, Integer> indexesHashMap = main.createIndexesHashMap(indexesSortedKeyWord);
//        char[] cypherTextCharArray = cypherText.toCharArray();
//        int pointerCypherTextCharArray = 0;
//        for (int i = 0; i < indexesSortedKeyWord.length; i++) {
//            String column = "";
//            //for cyklem projedu cypher array o delce bud delsi, nebo kratsi
//            int columnlength =  (main.containsArrayItem(longerColumnsIndexes,i)) ? longerColumnHeight : shorterColumnHeight;
//            for (int j = pointerCypherTextCharArray; j < pointerCypherTextCharArray + columnlength; j++) {
//                column += cypherTextCharArray[j];
//            }
//            pointerCypherTextCharArray += columnlength;
//            transpositionMatrix[indexesHashMap.get(i)] = column;
//        }
//        System.out.println("--------------TRANSPOSITION MATRIX-----------------------");
//        for (int k = 0; k < transpositionMatrix.length; k++) {
//            System.out.println(k + " " + transpositionMatrix[k]);
//        }
//        return transpositionMatrix;
//    }
//
//    public boolean containsArrayItem(final int[] array, final int v) {
//        boolean result = false;
//        for(int i : array){
//            if(i == v){
//                result = true;
//                break;
//            }
//        }
//        return result;
//    }
//
//
//}
