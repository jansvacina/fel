package bezpecnost;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * KAB
 * Solution for the problem: ADFGVX
 * October, 2017
 *
 * @author Jan Svacina
 */

class Bezpecnost {

	public static void main(String[] args) throws Exception {
		Bezpecnost inst = new Bezpecnost();
		while (inst.run()) {}
	}

	String type;

	boolean run() throws Exception {
	    type = nextToken();
	    if (type.equals("end")) return false;
	    if (type.equals("e")) encrypt();
	    if (type.equals("d")) decrypt();
	    return true;
	}

	StringTokenizer st = new StringTokenizer("");
	BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

	String nextToken () throws Exception {
		while (!st.hasMoreTokens()) st = new StringTokenizer (stdin.readLine());
		return st.nextToken();
	}

	String alphabet = "";
	String key = "";
	String text = "";
	final char[] adfgvx = {'A', 'D', 'F', 'G', 'V', 'X'};
	final String adfgvxString = "ADFGVX";

	void getProperties() throws Exception {
		clearProperties();
		String[] splits = nextToken().split("\\**QQQ\\**");
		alphabet = validateTokenStringNumbers(splits[0]).toUpperCase();
		key = validateTokenString(splits[1]).toUpperCase();
		while (st.hasMoreTokens()) text += validateTokenString(nextToken());
		text = text.toUpperCase();
	}

	void clearProperties() {
		alphabet = "";
		key="";
		text ="";
	}

	String validateTokenStringNumbers(String token) {
		token = token.replaceAll("[^a-zA-Z0-9]", "");
		return token;
	}

	String validateTokenString(String token) {
		token = token.replaceAll("[^a-zA-Z]", "");
		return token;
	}

	void encrypt() throws Exception {
		getProperties();
		ArrayList<String> matrix = createMatrix(alphabet);
		String cipherText = encryptPlainTextToCypherText(matrix, text);
		String keyWithoutDuplicates = removeDuplicatesFromKey(key);
		String[] columnsWithCipherText = createColumnsWithCipherText(keyWithoutDuplicates, cipherText);
		char[] keyWithoutDuplicatesSortedCharArray = keyWithoutDuplicates.toCharArray();
		for (int i = 0; i < keyWithoutDuplicatesSortedCharArray.length - 1; i++) {
            int j = i + 1;
            char tmpChar = keyWithoutDuplicatesSortedCharArray[j];
            String tmpString = columnsWithCipherText[j];
            while (j > 0 && tmpChar < keyWithoutDuplicatesSortedCharArray[j - 1]) {
                keyWithoutDuplicatesSortedCharArray[j] = keyWithoutDuplicatesSortedCharArray[j - 1];
                columnsWithCipherText[j] = columnsWithCipherText[j - 1];
                j--;
            }
            keyWithoutDuplicatesSortedCharArray[j] = tmpChar;
            columnsWithCipherText[j] = tmpString;
        }
		cipherText = mergeColumns(columnsWithCipherText);
		String cipherTextWithSpaces = insertSpacesToCipherTextByFive(cipherText);
		System.out.println(cipherTextWithSpaces);
	}

	void decrypt() throws Exception{
		getProperties();
		ArrayList<String> matrix = createMatrix(alphabet);
		String keyWithoutDuplicates = removeDuplicatesFromKey(key);
		text = removeWhiteSpaces(text);
		String sortedKey = sortKeyToAlphabeticOrder(keyWithoutDuplicates);
		int [] indexesSortedKey = indexesSortedKey(keyWithoutDuplicates, sortedKey);
		int[] longerColumnsIndexes = getLongerColumnsIndexes(indexesSortedKey, indexesSortedKey.length, text.length());
		String[] transpositionMatrix = createTranspositionMatrix(indexesSortedKey, longerColumnsIndexes, sortedKey, text);
		String cypherText = getCypherTextFromTranspositionMatrix(transpositionMatrix);
		String plainText = decryptCypherTextToPlainText(matrix, cypherText);
		System.out.println(plainText);
	}

	/**
	 *  HELPER METHODS
	 */

	String getCypherTextFromTranspositionMatrix(String[] transpositionMatrix) {
		String cypherText = "";
		for (int i = 0; i < transpositionMatrix[0].length() ; i++) {
			for (String s: transpositionMatrix
				 ) {
				if (s.length() != i ) cypherText += s.toCharArray()[i];
			}
		}
		return cypherText;
	}

	ArrayList<String> createMatrix(String alphabet) {
		int index = 0;
		ArrayList<String> matrix = new ArrayList<>();
		while (index < alphabet.length()) {
			matrix.add(alphabet.substring(index, index + adfgvx.length));
			index += adfgvx.length;
		}
		return matrix;
	}

	String encryptPlainTextToCypherText(ArrayList<String> matrix, String plainText) {
		String cipherText = "";
		for (char c : plainText.toUpperCase().toCharArray()
				) {
			for (int yIndex = 0; yIndex < matrix.size(); yIndex++) {
				int xIndex = matrix.get(yIndex).indexOf(c);
				if (xIndex != -1) {
					cipherText += adfgvx[yIndex];
					cipherText += adfgvx[xIndex];
				}
			}
		}
		return cipherText;
	}

	String decryptCypherTextToPlainText(ArrayList<String> matrix, String cypherText) {
		String plainText = "";
		char[] cypherTextCharArray = cypherText.toCharArray();
		for (int i = 0; i < cypherTextCharArray.length -1; i = i + 2) {
			int yAxis = adfgvxString.indexOf(cypherTextCharArray[i]);
			int xAxis = adfgvxString.indexOf(cypherTextCharArray[i+1]);
			String row = matrix.get(yAxis);
			char character = row.charAt(xAxis);
			plainText += character;
		}
		return plainText;
	}

	String removeDuplicatesFromKey(String key) {
		char[] charKey = key.toCharArray();
		String keyWithoutDuplicates = "";
		for (int i = 0; i < charKey.length; i++) {
			if (keyWithoutDuplicates.indexOf(charKey[i]) == -1)
				keyWithoutDuplicates += charKey[i];
		}
		return keyWithoutDuplicates;
	}

	String[] createColumnsWithCipherText(String keyWithoutDuplicates, String cipherText) {
		String[] columnsWithCipherText = new String[keyWithoutDuplicates.length()];
		for (int i = 0; i < columnsWithCipherText.length; i++
				) {
			columnsWithCipherText[i] = "";
		}
		int index = 0;
		String string = "";
		for (char c : cipherText.toCharArray()
				) {
			string = columnsWithCipherText[index];
			string += c;
			columnsWithCipherText[index] = string;
			index++;
			if (keyWithoutDuplicates.toCharArray().length == index) {
				index = 0;
			}
		}
		return columnsWithCipherText;
	}

	String mergeColumns(String[] columnsWithCipherText ){
		String cipherText = "";
		for (String s : columnsWithCipherText
				) {
			cipherText += s;
		}

		return cipherText;
	}

	String insertSpacesToCipherTextByFive(String cipherText){
		String cipherTextWithSpaces = "";
		int index = 1;
		for (char c : cipherText.toCharArray()
				) {
			cipherTextWithSpaces += c;
			if (index % 5 == 0) {
				cipherTextWithSpaces += " ";
			}
			index++;
		}
		cipherTextWithSpaces.trim();
		return cipherTextWithSpaces;
	}

	String sortKeyToAlphabeticOrder(String key){
		char[] keyCharArray = key.toCharArray();
		for (int i = 0; i < keyCharArray.length - 1; i++) {
			int j = i + 1;
			char tmpChar = keyCharArray[j];
			while (j > 0 && tmpChar < keyCharArray[j - 1]) {
				keyCharArray[j] = keyCharArray[j - 1];
				j--;
			}
			keyCharArray[j] = tmpChar;
		}
		key = "";
		for (char c: keyCharArray
				) {
			key += c;
		}
		return key;
	}

	String removeWhiteSpaces(String key){
		return key.replaceAll("\\s+", "");
	}

	int[] indexesSortedKey(String key, String sortedKey) {
//		int[] indexesSortedKey = new int[sortedKey.length()];
//
//		for (int i = 0; i < key.length(); i++
//				) {
//			char k = key.toCharArray()[i];
//			int j = sortedKey.indexOf(k);
//			indexesSortedKey[i] = j;
//		}
//		return indexesSortedKey;
		int[] indexesSortedKey = new int[sortedKey.length()];
		HashMap<Character, Integer> hmap = new HashMap<Character, Integer>();
		for (int i = 0; i < sortedKey.length(); i++) {
			hmap.put(sortedKey.charAt(i), i);
		}
//		Map<Character, Integer> map = sortByValues(hmap);
//		Set set2 = map.entrySet();
//		Iterator iterator2 = set2.iterator();
//		int counter = 0;
//		while(iterator2.hasNext()) {
//			Map.Entry me2 = (Map.Entry)iterator2.next();
//			indexesSortedKey[counter] = (int) me2.getKey();
//			counter++;
//		}
		for (int i = 0; i < key.length(); i++) {
			indexesSortedKey[i] = hmap.get((Character) key.toCharArray()[i]);
		}
		return indexesSortedKey;
	}

	private static HashMap sortByValues(HashMap map) {
		List list = new LinkedList(map.entrySet());
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue())
						.compareTo(((Map.Entry) (o2)).getValue());
			}
		});
		HashMap sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put(entry.getKey(), entry.getValue());
		}
		return sortedHashMap;
	}


	int[] getLongerColumnsIndexes(int[] indexes, int sortedKeyLength, int cypherTextLength) {
		int countIndexes = cypherTextLength / sortedKeyLength;
		countIndexes = cypherTextLength - (countIndexes * sortedKeyLength);
		int[] longerColumnsIndexes = new int[countIndexes];
		for (int i = 0; i < countIndexes; i++ ) {
			longerColumnsIndexes[i] = indexes[i];
		}
		return longerColumnsIndexes;
	}

	int getLongerColumnHeight(int sortedKeyLength, int cypherTextLength) {
		return (cypherTextLength / sortedKeyLength) + 1;
	}

	int getShorterColumnHeight(int sortedKeyLength, int cypherTextLength) {
		return cypherTextLength / sortedKeyLength;
	}

	HashMap<Integer, Integer> createIndexesHashMap(int[] indexesSortedKeyWord) {
		HashMap<Integer, Integer> hashMap = new HashMap<>();
		for (int i = 0; i < indexesSortedKeyWord.length; i++) {
			hashMap.put(indexesSortedKeyWord[i], i);
		}
		return hashMap;
	}

	String[] createTranspositionMatrix(int[] indexesSortedKeyWord, int[] longerColumnsIndexes, String sortedKeyWord, String text) {
		String[] transpositionMatrix = new String[indexesSortedKeyWord.length];
		for (String s: transpositionMatrix) { s = "";}
		int shorterColumnHeight = getShorterColumnHeight(sortedKeyWord.length(), text.length());
		int longerColumnHeight = getLongerColumnHeight(sortedKeyWord.length(), text.length());
		HashMap<Integer, Integer> indexesHashMap = createIndexesHashMap(indexesSortedKeyWord);
		char[] cypherTextCharArray = text.toCharArray();
		int pointerCypherTextCharArray = 0;
		for (int i = 0; i < indexesSortedKeyWord.length; i++) {
			String column = "";
			int columnlength =  (containsArrayItem(longerColumnsIndexes,i)) ? longerColumnHeight : shorterColumnHeight;
			for (int j = pointerCypherTextCharArray; j < pointerCypherTextCharArray + columnlength; j++) {
				column += cypherTextCharArray[j];
			}
			pointerCypherTextCharArray += columnlength;
			transpositionMatrix[indexesHashMap.get(i)] = column;
		}
		return transpositionMatrix;
	}

	boolean containsArrayItem(int[] array, int integer) {
		boolean contains = false;
		for (int i : array
				){
			if(i == integer){
				contains = true;
				break;
			}
		}
		return contains;
	}
}
