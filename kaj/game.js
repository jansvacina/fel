/**
 * This namespace provides functionality for the actual game
 */
var Drawing;
(function (Drawing) {
    /**
     * Global properties
     */
    var canvas;
    var ctx;
    var game;
    var globalID;
    var overallScoreComponent = document.querySelector("#_overallScore");
    var gameDifficultyComponent = document.querySelector('#_difficulty');
    var pointsComponent = document.querySelector('#_points');
    var livesComponent = document.querySelector("#_lives");
    /**
     *  Listening on the event of restarting the game and the actual starter
     */
    var restartButton = document.querySelector('#_restart_bttn');
    restartButton.addEventListener('click', function (e) {
        starter();
    });
    function starter() {
        restartButton.style.display = 'none';
        pointsComponent.innerHTML = "Points: " + 0;
        livesComponent.innerHTML = "Lives: " + 5;
        overallScoreComponent.innerHTML;
        canvas = document.getElementById('cnvs');
        ctx = canvas.getContext("2d");
        game = new Game();
        var myCircle = new cCircle(100, 300, 10);
        var circle2 = new cCircle(1200, 300, 10, "blue", 2);
        gameLoop();
    }
    Drawing.starter = starter;
    /**
     * Game class provides main game logic conducting on the canvas board
     */
    var Game = (function () {
        function Game() {
            var _this = this;
            this.velocity = 4;
            this.lives = 5;
            this.points = 0;
            this.difficulty = 1;
            this.myCircle = new cCircle(100, 300, 10);
            this.obstacles = [];
            this.circle2 = new cCircle(1200, 300, 10, "blue", 2);
            this.drawAll = function () {
                _this.myCircle.draw();
                for (var _i = 0, _a = _this.obstacles; _i < _a.length; _i++) {
                    var circle = _a[_i];
                    circle.draw();
                }
            };
            this.displayScore = function () {
                game.points = game.points + 1 / 10;
                var pointsToDisplay = Math.round(game.points);
                pointsComponent.innerHTML = "Points: " + pointsToDisplay.toString();
            };
            this.overrideLives = function () {
                game.lives--;
                livesComponent.innerHTML = "Lives: " + game.lives.toString();
            };
            this.obstacles.push(this.circle2);
            this.myCircle.enableGravity();
        }
        ;
        return Game;
    }());
    /**
     * Provides computitation logic, if the collision occured
     */
    var Collision = (function () {
        function Collision() {
        }
        Collision.isCollision = function (p1, p2) {
            var vx = p2.x - p1.x;
            var vy = p2.y - p1.y;
            var midpointDistanceX = p2.x - p1.x;
            var midpointDistanceY = p2.y - p1.y;
            var radiusTotal = p2.radius + p1.radius;
            return (midpointDistanceX < radiusTotal) && (midpointDistanceY < radiusTotal) ? true : false;
        };
        Collision.passedAway = function (p1) {
            return (p1.x < 50) ? true : false;
        };
        return Collision;
    }());
    /**
     * Repeatedly updates canvas board and triggers events based on the
     * mutual distribution of actual items
     */
    function gameLoop() {
        globalID = requestAnimationFrame(gameLoop);
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, 1280, 720);
        for (var _i = 0, _a = game.obstacles; _i < _a.length; _i++) {
            var circ = _a[_i];
            if ((circ.x = circ.x - (game.velocity * game.difficulty)) >= 1280 + circ.radius) {
                circ.x = -circ.radius;
            }
        }
        game.difficulty = Math.ceil(game.points / 100);
        gameDifficultyComponent.innerHTML = "Difficulty: " + game.difficulty;
        game.displayScore();
        game.drawAll();
        for (var _b = 0, _c = game.obstacles; _b < _c.length; _b++) {
            var circ = _c[_b];
            if (Collision.isCollision(game.myCircle, circ)) {
                var audio = new Audio('bum.wav');
                audio.play();
                game.overrideLives();
                game.obstacles.pop();
                var tmp = new cCircle(1200, 300, 10, "blue", 2);
                game.obstacles.push(tmp);
                tmp.draw();
            }
            else if (Collision.passedAway(circ)) {
                game.obstacles.pop();
                var tmp = new cCircle(1200, 300, 10, "blue", 2);
                game.obstacles.push(tmp);
                tmp.draw();
            }
        }
        if (game.lives < 1) {
            cancelAnimationFrame(globalID);
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, 1280, 720);
            game.obstacles = [];
            var tmpPoints = localStorage.getItem('points');
            game.points += parseInt(tmpPoints);
            var tmpNumberPoint = Math.round(game.points);
            localStorage.setItem("points", tmpNumberPoint.toString());
            overallScoreComponent.innerHTML = localStorage.getItem('points');
            game.lives = 5;
            game.points = 0;
            restartButton.style.display = 'block';
        }
    }
    Drawing.gameLoop = gameLoop;
    /**
     * Provides properties and methods to handle a circle object properly
     */
    var cCircle = (function () {
        function cCircle(x, y, radius, color, line_width) {
            if (color === void 0) { color = "red"; }
            if (line_width === void 0) { line_width = 2; }
            var _this = this;
            this.x = 0;
            this.y = 0;
            this.radius = 10;
            this.lineWidth = 2;
            this.color = "red";
            this.gravity = 0;
            this.gravitySpeed = 0;
            this.jumping = false;
            this.jumpEvent = null;
            this.down = false;
            this.draw = function () {
                ctx.save();
                ctx.beginPath();
                ctx.strokeStyle = _this.color;
                ctx.lineWidth = _this.lineWidth;
                ctx.arc(_this.x, _this.y, _this.radius, 0, 2 * Math.PI);
                ctx.stroke();
                ctx.restore();
            };
            this.enableGravity = function () {
                document.addEventListener("keydown", _this.jump);
            };
            this.jump = function () {
                if (!_this.jumping) {
                    _this.jumping = true;
                    _this.y = _this.y - 25;
                    setTimeout(_this.land, 1000);
                }
            };
            this.land = function () {
                _this.y += 25;
                setTimeout(_this.dock, 250);
            };
            this.dock = function () {
                _this.jumping = false;
            };
            this.x = x;
            this.y = y;
            this.radius = radius;
            this.color = color;
            this.lineWidth = line_width;
        }
        return cCircle;
    }());
})(Drawing || (Drawing = {}));
/**
 * File is source of page logic delegation: routing, saving user data
 */
var Drawing;
(function (Drawing) {
    /**
     * geolocation working asynchronously, thus there is a method
     * for optimal and error flow
     */
    var geolocationOk = function (position) {
        localStorage.setItem("latitude", position.coords.latitude);
        localStorage.setItem("longitude", position.coords.longitude);
        var footer = document.querySelector("#geolocation");
        footer.innerHTML = " The player "
            + localStorage.getItem("name")
            + " is on the position of latitude "
            + localStorage.getItem("latitude")
            + " and longitude "
            + localStorage.getItem("longitude")
            + ".";
    };
    var geolocationError = function (e) {
        console.log(e.message);
        var footer = document.querySelector("#_header");
        footer.innerHTML = " Current player " + localStorage.getItem("name");
    };
    /**
     * Processing inserting data to local storage
     */
    var saveUser = function (e) {
        var nameInput = document.querySelector('#_name');
        if (nameInput.checkValidity()) {
            e.preventDefault();
            localStorage.setItem("name", nameInput.value);
            localStorage.setItem("points", "0");
            localStorage.setItem("overallScore", "0");
            var form = document.querySelector('#_form');
            form.style.display = "none";
            var scoreComponent = document.querySelector('#_overallScore');
            scoreComponent.innerHTML = localStorage.getItem("overallScore");
            var nameComponent = document.querySelector("#_overallName");
            nameComponent.innerHTML = "Name: " + localStorage.getItem("name");
            navigator.geolocation.getCurrentPosition(geolocationOk, geolocationError);
            Drawing.starter();
        }
    };
    var _submit_bttn = document.querySelector('#_submit_bttn');
    _submit_bttn.addEventListener('click', function (e) {
        saveUser(e);
    });
    /** Methods for history API */
    window.addEventListener('popstate', function (e) {
        var componentId = "#_" + e.state + "_bttn";
        var component = document.querySelector(componentId);
        component.checked = true;
    });
    var historyListerner = function (e) {
        if (e.target != e.currentTarget) {
            history.pushState(e.target.dataset.name, null, e.target.dataset.name);
        }
        return e;
    };
    /**
     * Listeners processing navigation in the app
     */
    var _play_bttn = document.querySelector('#_play_bttn');
    var _score_bttn = document.querySelector('#_score_bttn');
    var _about_bttn = document.querySelector('#_about_bttn');
    _play_bttn.addEventListener('click', function (e) { historyListerner(e); });
    _score_bttn.addEventListener('click', function (e) { historyListerner(e); });
    _about_bttn.addEventListener('click', function (e) { historyListerner(e); });
    var scoreComponent = document.querySelector('#_overallScore');
    scoreComponent.innerHTML = "0";
    /**
     * Method for checking the state of application, offline and online
     */
    window.addEventListener('load', function () {
        var status = document.getElementById("status");
        function updateOnlineStatus(event) {
            var condition = navigator.onLine ? "online" : "offline";
            status.innerHTML = condition.toUpperCase();
            return event;
        }
        window.addEventListener('online', updateOnlineStatus(event));
        window.addEventListener('offline', updateOnlineStatus(event));
    });
    /**
     * Method for manipulating with svg object
     */
    var svgElement = document.getElementById("rect1");
    svgElement.addEventListener("mouseover", mouseOver);
    function mouseOver() {
        var svg = document.getElementById("innerCircle");
        svg.setAttributeNS(null, "stroke", "blue");
    }
})(Drawing || (Drawing = {}));
//# sourceMappingURL=game.js.map