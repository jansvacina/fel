/**
 * Created by jan on 04-Jun-17.
 */
namespace Drawing{
    export interface iShape {
        draw(): void;
        x: number;
        y: number;
        color: string;
        lineWidth: number;
    }
}