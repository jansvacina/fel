/**
 * File is source of page logic delegation: routing, saving user data
 */
namespace Drawing{

    /** 
     * geolocation working asynchronously, thus there is a method
     * for optimal and error flow
     */

    var geolocationOk = function(position) {
        localStorage.setItem("latitude",position.coords.latitude);
        localStorage.setItem("longitude", position.coords.longitude);
        var footer = document.querySelector("#geolocation");
        footer.innerHTML = " The player "
            + localStorage.getItem("name")
            + " is on the position of latitude "
            + localStorage.getItem("latitude")
            + " and longitude "
            + localStorage.getItem("longitude")
            + ".";
    }

    var geolocationError = function(e) {
        console.log(e.message);
        var footer = document.querySelector("#_header");
        footer.innerHTML = " Current player " + localStorage.getItem("name");
    }

    /**
     * Processing inserting data to local storage
     */


    var saveUser = function(e){
        var nameInput = document.querySelector('#_name');
        if(nameInput.checkValidity()){
            e.preventDefault();
            localStorage.setItem("name", nameInput.value);
            localStorage.setItem("points", "0");
            localStorage.setItem("overallScore", "0");
            var form = document.querySelector('#_form');
            form.style.display = "none";
            var scoreComponent = document.querySelector('#_overallScore');
            scoreComponent.innerHTML = localStorage.getItem("overallScore");
            var nameComponent = document.querySelector("#_overallName");
            nameComponent.innerHTML = "Name: " + localStorage.getItem("name");
            navigator.geolocation.getCurrentPosition(geolocationOk, geolocationError);
            Drawing.starter();
        }
    }

    var _submit_bttn = document.querySelector('#_submit_bttn');
    _submit_bttn.addEventListener('click',function (e) {
        saveUser(e);
    });

    /** Methods for history API */

    window.addEventListener('popstate', (e) => {
        var componentId = "#_" + e.state + "_bttn";
        var component = document.querySelector(componentId);
        component.checked = true;
    });

    var historyListerner = function(e){
        if(e.target != e.currentTarget){
            history.pushState(e.target.dataset.name, null, e.target.dataset.name);
        }
        return e;
    }

    /**
     * Listeners processing navigation in the app
     */

    var _play_bttn = document.querySelector('#_play_bttn');
    var _score_bttn = document.querySelector('#_score_bttn');
    var _about_bttn = document.querySelector('#_about_bttn');
    _play_bttn.addEventListener('click', function(e){historyListerner(e)});
    _score_bttn.addEventListener('click', function(e){historyListerner(e)});
    _about_bttn.addEventListener('click', function(e){historyListerner(e)});
    var scoreComponent = document.querySelector('#_overallScore');
    scoreComponent.innerHTML = "0";
    
    /**
     * Method for checking the state of application, offline and online
     */

    window.addEventListener('load', function() {
        var status = document.getElementById("status");
        function updateOnlineStatus(event) {

            var condition = navigator.onLine ? "online" : "offline";

            status.innerHTML = condition.toUpperCase();

            return event;
        }

        window.addEventListener('online',  updateOnlineStatus(event));
        window.addEventListener('offline', updateOnlineStatus(event));
    });

    /**
     * Method for manipulating with svg object
     */

    var svgElement = document.getElementById("rect1");
    svgElement.addEventListener("mouseover", mouseOver);
    function mouseOver() {
        var svg = document.getElementById("innerCircle");    
        svg.setAttributeNS(null, "stroke", "blue");
    }
}




