///<reference path="iShape.ts"/>
import iShape = Drawing.iShape;

/**
 * This namespace provides functionality for the actual game
 */

namespace Drawing{
    /**
     * Global properties
     */
    var canvas: HTMLCanvasElement;
    var ctx: CanvasRenderingContext2D;
    var game: Game;
    var globalID: any;
    
    var overallScoreComponent = document.querySelector("#_overallScore");
    var gameDifficultyComponent = document.querySelector('#_difficulty');
    var pointsComponent = document.querySelector('#_points');
    var livesComponent = document.querySelector("#_lives");
    
    /**
     *  Listening on the event of restarting the game and the actual starter
     */

    var restartButton = document.querySelector('#_restart_bttn');
    restartButton.addEventListener('click', function(e){
        starter();
    });

    export function starter() {
        restartButton.style.display = 'none';
        pointsComponent.innerHTML = "Points: " + 0;
        livesComponent.innerHTML = "Lives: " + 5;
        overallScoreComponent.innerHTML
        canvas = <HTMLCanvasElement>document.getElementById('cnvs');
        ctx = canvas.getContext("2d");
        game = new Game();
        var myCircle: cCircle = new cCircle(100, 300, 10);
        var circle2: cCircle = new cCircle(1200, 300, 10, "blue", 2);
        gameLoop();
    }

    /**
     * Game class provides main game logic conducting on the canvas board
     */

    class Game{
        public velocity: number = 4;
        public lives: number = 5;
        public points: number = 0;
        public difficulty: number = 1;
        
        public myCircle: cCircle = new cCircle(100, 300, 10);
        public obstacles: cCircle[] = [];
        public circle2: cCircle = new cCircle(1200, 300, 10, "blue", 2);
        constructor(){
            this.obstacles.push(this.circle2);
            this.myCircle.enableGravity();
        };
        public drawAll = (): void =>{
            this.myCircle.draw();
            for(var circle of this.obstacles){
                circle.draw();
            }
        }
        public displayScore = (): void =>{
            game.points = game.points + 1/10;
            var pointsToDisplay = Math.round(game.points);
            pointsComponent.innerHTML = "Points: " + pointsToDisplay.toString();
        }

        public overrideLives = (): void =>{
            game.lives--;
            livesComponent.innerHTML = "Lives: " + game.lives.toString();
        }
    }

    /**
     * Provides computitation logic, if the collision occured
     */

    class Collision {
        public static isCollision(p1: cCircle, p2: cCircle): boolean {
            var vx = p2.x - p1.x;
            var vy = p2.y - p1.y;
            var midpointDistanceX = p2.x - p1.x;
            var midpointDistanceY = p2.y - p1.y;
            var radiusTotal = p2.radius + p1.radius;
            return (midpointDistanceX < radiusTotal)&&(midpointDistanceY < radiusTotal) ? true : false;
        }
        public static passedAway(p1: cCircle): boolean {

            return (p1.x < 50) ? true: false;
        }
    }

    /**
     * Repeatedly updates canvas board and triggers events based on the
     * mutual distribution of actual items
     */

    export function gameLoop() {
        globalID = requestAnimationFrame(gameLoop);
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, 1280, 720);

        for(var circ of game.obstacles){
            if ((circ.x = circ.x - (game.velocity*game.difficulty)) >= 1280 + circ.radius) {
                circ.x = -circ.radius;
            }
        }
        
        game.difficulty = Math.ceil(game.points/100);
        
        gameDifficultyComponent.innerHTML = "Difficulty: " + game.difficulty;
        
        game.displayScore();
        game.drawAll();

        for(var circ of game.obstacles){
            if(Collision.isCollision(game.myCircle,circ)){
                var audio = new Audio('bum.wav');
                audio.play();
                game.overrideLives();
                game.obstacles.pop();
                var tmp: cCircle = new cCircle(1200, 300, 10, "blue", 2);
                game.obstacles.push(tmp);
                tmp.draw()
            } else if (Collision.passedAway(circ)){
                game.obstacles.pop();
                var tmp: cCircle = new cCircle(1200, 300, 10, "blue", 2);
                game.obstacles.push(tmp);
                tmp.draw()
            }
        }
        if(game.lives < 1){
            cancelAnimationFrame(globalID);
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, 1280, 720);
            game.obstacles = [];
            var tmpPoints = localStorage.getItem('points');
            game.points += parseInt(tmpPoints);
            var tmpNumberPoint = Math.round(game.points);
            localStorage.setItem("points", tmpNumberPoint.toString());
            overallScoreComponent.innerHTML = localStorage.getItem('points');
            game.lives = 5;
            game.points = 0;
            restartButton.style.display = 'block';
        }
    }

    /**
     * Provides properties and methods to handle a circle object properly
     */

    class cCircle implements iShape {
        public x: number = 0;
        public y: number = 0;
        public radius: number = 10;
        public lineWidth: number = 2;
        public color: string = "red";
        public gravity = 0;
        public gravitySpeed = 0;

        public jumping: boolean = false;
        public jumpEvent: any = null;
        public down: any = false;

        constructor(x: number, y: number, radius: number, color: string = "red", line_width: number = 2)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
            this.color = color;
            this.lineWidth = line_width;
        }
        public draw = (): void => {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.lineWidth;
            ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.restore();
        }

        public enableGravity = (): void => {
            document.addEventListener("keydown", this.jump);
        }

        public jump = (): void => {
            if (!this.jumping) {
                this.jumping = true;
                this.y = this.y - 25;
                setTimeout(this.land, 1000);
            }
        }

        public land = (): void => {
            this.y += 25;
            setTimeout(this.dock,250);
        }

        public dock = (): void => {
            this.jumping = false;
        }
    }
}