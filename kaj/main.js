/**
 * Created by jan on 10-Jun-17.
 */
var Drawing;
(function (Drawing) {
    window.addEventListener('popstate', function (e) {
        var componentId = "#_" + e.state + "_bttn";
        var component = document.querySelector(componentId);
        component.checked = true;
    });
    var geolocationOk = function (position) {
        localStorage.setItem("latitude", position.coords.latitude);
        localStorage.setItem("longitude", position.coords.longitude);
        var footer = document.querySelector("#_footer");
        footer.innerHTML = "The player "
            + localStorage.getItem("name")
            + " is on the position of latitude "
            + localStorage.getItem("latitude")
            + " and longitude "
            + localStorage.getItem("longitude")
            + ".";
    };
    var geolocationError = function (e) {
        console.log(e.message);
    };
    var historyListerner = function (e) {
        history.pushState(e.target.dataset.name, null, e.target.dataset.name);
        return e;
    };
    var saveUser = function (e) {
        var nameInput = document.querySelector('#_name');
        if (nameInput.checkValidity()) {
            e.preventDefault();
            localStorage.setItem("name", nameInput.value);
            var form = document.querySelector('#_form');
            form.style.display = "none";
            navigator.geolocation.getCurrentPosition(geolocationOk, geolocationError);
            Drawing.starter();
        }
    };
    var _submit_bttn = document.querySelector('#_submit_bttn');
    _submit_bttn.addEventListener('click', function (e) {
        saveUser(e);
    });
    var _play_bttn = document.querySelector('#_play_bttn');
    var _score_bttn = document.querySelector('#_score_bttn');
    var _about_bttn = document.querySelector('#_about_bttn');
    _play_bttn.addEventListener('click', function (e) { historyListerner(e); });
    _score_bttn.addEventListener('click', function (e) { historyListerner(e); });
    _about_bttn.addEventListener('click', function (e) { historyListerner(e); });
})(Drawing || (Drawing = {}));
//# sourceMappingURL=main.js.map