package robot;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Scanner;

class Server {
    public static void svr_main(int port) throws IOException {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.err.println("Could not listen on port: " + port);
        }
        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                System.err.println("Accept failed.");
            }
            new Thread(new ServerConnection(clientSocket)).start();
        }
    }
}
class ServerConnection implements Runnable {
    private Socket clientSocket;
    private final String OK = "202 OK\r\n";
    private final String LOGIN = "200 LOGIN\r\n";
    private final String PASSWORD = "201 PASSWORD\r\n";
    private final String BAD_CHECKSUM = "300 BAD CHECKSUM\r\n";
    private final String LOGIN_FAILED = "500 LOGIN FAILED\r\n";
    private final String SYNTAX_ERROR = "501 SYNTAX ERROR\r\n";
    private final String TIMEOUT = "502 TIMEOUT\r\n";
    private boolean closed = false;
    public ServerConnection(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }
    @Override
    public void run() {
        /**
         * INITIALIZATION AND SETUP
         */
        BufferedInputStream bis = null;
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter bw = new BufferedWriter(pw);
        /**
         * AUTHORIZATION
         */
        boolean authorized = false;
        boolean authenticate = false;
        int loginSum = 0; //here is login stored
        boolean isLoginCorrect = true;
        String robotWord = "Robot";
        char[] robotWordChars = robotWord.toCharArray();
        Integer loginCounter = 0;
        String passwordSum = "";
        String connectionNotification = ("client accepted from: " + clientSocket.getInetAddress()
                + ":" + clientSocket.getPort());
        try {
            bis = new BufferedInputStream(clientSocket.getInputStream());
        } catch (IOException e) {
            System.err.println("Couldn't get I/O.");
        }
        sendMessage(bw, LOGIN, connectionNotification);
        try {
            Thread.sleep(210);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /**
         * THREAD WHILE LOOP
         */
        long start = System.currentTimeMillis();
        long end = start + 45 * 1000; //45
        while (System.currentTimeMillis() < end) {
            long counter = 0;
            int j = 0;
            String info = "";
            String infoFlag = "";
            boolean isInfoCorrect = false;
            String fotoFlag = "";
            String foto = "";
            boolean isFotoCorrect = false;
            String messageToPrintToClient = OK;
            /**
             * INPUT READING WHILE LOOP
             */
            int i;
            while (true) {
                try {
                    if (!closed && bis.available() != 0) {
                        i = bis.read(); //reading bytes
                        if (j == 13 && i == 10) {
                            break;
                        }
                        if (!authorized && i != 13) {
                            if (loginCounter < 5) {
                                if (robotWordChars[loginCounter] != (char) i) {
                                    isLoginCorrect = false;
                                    messageToPrintToClient = LOGIN_FAILED;
                                }
                            }
                            loginCounter++;
                            loginSum += i;
                        }
                        if (authorized && !authenticate && i!= 13) {
                            char ch = (char) i;
                            passwordSum += ch;
                        }
                        /**
                         * AUTHORIZED AND AUTHENTICATE
                         */
                        if (authorized && authenticate) {
                            if (infoFlag.length() < 5) {
                                if (i == 13) {
                                    isInfoCorrect = false;
                                    isFotoCorrect = false;
                                    messageToPrintToClient = SYNTAX_ERROR;
                                    sendMessage(bw,messageToPrintToClient,messageToPrintToClient);
                                    quitConnection(bw, bis);
                                }
                                info += (char) i;
                                infoFlag += (char) i;
                                foto += (char) i;
                                fotoFlag += (char) i;
                            } else {
                                if (infoFlag.equals("INFO ")) {
                                    info += (char) i;
                                    isInfoCorrect=true;
                                } else if (fotoFlag.equals("FOTO ")) {
                                    messageToPrintToClient = parseFoto(bis, bw, foto, i);
                                    if( closed){
                                        return;
                                    }
                                    if ( messageToPrintToClient == SYNTAX_ERROR) {
                                        isFotoCorrect = false;
                                        isInfoCorrect = false;
                                        sendMessage(bw,SYNTAX_ERROR,SYNTAX_ERROR);
                                        quitConnection(bw,bis);
                                    }
                                    if(messageToPrintToClient == OK){
                                        isFotoCorrect = true;
                                    }
                                    break;
                                } else {
                                    info += (char) i;
                                    messageToPrintToClient = SYNTAX_ERROR;
                                    isInfoCorrect = false;
                                    isFotoCorrect = false;
                                }
                            }
                        }
                        j = i; //adding previous value
                    } else {
                        try {
                            if (counter > 100 && info.length() != 0) {
                                break;
                            }
                            Thread.sleep(100);
                            counter++;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            /**
             * PRINTING MESSAGES AFTER READING INPUT IS DONE
             */
            if (authorized && authenticate) {
                if (isInfoCorrect || isFotoCorrect) {
                    sendMessage(bw, messageToPrintToClient, "valid message");
                } else if(messageToPrintToClient == BAD_CHECKSUM){
                    sendMessage(bw, messageToPrintToClient, messageToPrintToClient);
                } else if (!closed && !isInfoCorrect || !isFotoCorrect) {
                    messageToPrintToClient=SYNTAX_ERROR;
                    sendMessage(bw, messageToPrintToClient, "info incorrectly sent, closing connection");
                    quitConnection(bw, bis);
                    break;
                }
            }
            if (authorized && !authenticate) {
                    if (isLoginCorrect && loginCounter > 5 && (loginSum == Integer.parseInt(passwordSum))) {
                        sendMessage(bw, messageToPrintToClient, "client authenticated");
                        authenticate = true;
                    } else {
                        sendMessage(bw, LOGIN_FAILED, "invalid credentials - connection stopped");
                        break;
                    }
            }
            if (!authorized) {
                sendMessage(bw, PASSWORD, "client authorized");
                authorized = true;
            }
        }
        if (System.currentTimeMillis() >= end) {
            sendMessage(bw, TIMEOUT, TIMEOUT);
        }
        quitConnection(bw, bis);
        return;
    }
    /*
    METHODS
     */
    public void sendMessage(BufferedWriter bw, String message, String messageServer) {
        printServer(messageServer);
        printClient(message);
        if (!closed) {
            try {
                bw.write(message);
                bw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void quitConnection(BufferedWriter bw, BufferedInputStream bis) {
        closed = true;
        try {
            bis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void printServer(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append("===>: ");
        sb.append(clientSocket.getInetAddress()
                + ":" + clientSocket.getPort() + " | ");
        sb.append(message);
        System.out.println(sb.toString());
    }
    public void printClient(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append("<===: ");
        sb.append(clientSocket.getInetAddress()
                + ":" + clientSocket.getPort() + " | ");
        sb.append(message);
        System.out.println(sb.toString());
    }
    public String parseFoto(BufferedInputStream bis, BufferedWriter bw, String foto, int k) throws IOException, InterruptedException {
        String numberBytes = "";
        numberBytes += (char) k;
        int controlSum = 0;
        boolean toBeContinued = true;
        int j = 0;
        int lengthCounter = 0;
        /*
        GET NUMBER OF BYTES
         */
        while (true) {
            try {
                if (!closed && bis.available() != 0) {
                    int i;
                    i = bis.read(); //reading bytes
                    if (j == 13 && i == 10) {
                        sendMessage(bw,SYNTAX_ERROR,SYNTAX_ERROR);
                        quitConnection(bw,bis);
                        return "";
                    }
                    if ((byte) i == 32) {
                        break;
                    }
                    numberBytes += (char) i;
                    lengthCounter++;
                    j = i;
                } else {
                    lengthCounter++;
                    Thread.sleep(20);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(lengthCounter > 40){
                sendMessage(bw,SYNTAX_ERROR,SYNTAX_ERROR);
                quitConnection(bw,bis);
                return "";
            }
        }

        j = 0;
        Integer bytesCounter = 0;
        while (bytesCounter < Integer.parseInt(numberBytes)) {
            try {
                if (!closed && bis.available() != 0) {
                    int i;
                    i = bis.read();
                    controlSum += i;
                    bytesCounter++;
                } else {
                    Thread.sleep(20);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /*
        GET CHECKSUM
         */
        int c = 0;
        int hexCheckSum;
        byte[] array = new byte[4];
        if (toBeContinued) {
            for (int i = 0; i < 4; i++) {
                array[i] = (byte) bis.read();
            }
        }
        hexCheckSum = ByteBuffer.wrap(array).getInt();


        /*
        COMPARE CHECKSUM, RETRIVE MESSAGE
         */

        if (hexCheckSum == controlSum) {
            return OK;
        } else {
            return BAD_CHECKSUM;
        }
    }
}
//class Client {
//    public static void cli_main( int port, String servername) throws IOException {
//        Socket echoSocket = null;
//        PrintWriter out = null;
//        BufferedReader in = null;
//
//        try {
//            echoSocket = new Socket( servername, port);
//            out = new PrintWriter(echoSocket.getOutputStream(), true);
//            in = new BufferedReader(new InputStreamReader(
//                    echoSocket.getInputStream()));
//        } catch (UnknownHostException e) {
//            System.err.println("Don't know about host: " + servername);
//            System.exit(1);
//        } catch (IOException e) {
//            System.err.println("Couldn't get I/O for " + servername);
//            System.exit(1);
//        }
//        String userInput = "ahoj";
//        System.out.println("sending: " + userInput);
//        out.println(userInput);
//        System.out.println("echo: " + in.readLine());
//        System.out.println("socket: " + echoSocket.toString());
//
//        out.close();
//        in.close();
//        echoSocket.close();
//    }
//}
//
//public class Robot {
//    public static void main(String[] args) throws IOException {
//        if (args.length == 0) {
//            System.err.println("Client: java robot.Robot <hostname> <port>");
//            System.err.println("Server: java robot.Robot <port>");
//            System.exit(1);
//        }
//        if (args.length > 1) {
//            System.out.println("Starting client...\n");
//            Client client = new Client();
//            client.cli_main( Integer.parseInt( args[1]), args[0]);
//        } else {
//            System.out.println("Starting server...\n");
//            Server server = new Server();
//            server.svr_main( Integer.parseInt( args[0]));
//        }
//    }
//}



class ServerStarter {
    public static void main(String[] args) throws IOException {
        System.out.println("ServerStarter: starting...\n");
        Server server = new Server();
        server.svr_main(3999);
    }
}

class ClientStarter {
    public static void main(String[] args) throws IOException {
        System.out.println("ClientStarter");
        Socket echoSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            echoSocket = new Socket("localhost", 3999);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                    echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + "localhost");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + "localhost");
            System.exit(1);
        }
        Scanner sc = new Scanner(System.in);
        boolean communicate = true;
        while (communicate) {
            System.out.println("Server response: " + in.readLine());
            System.out.println("Respond: ");
            String userInput = sc.nextLine();
            out.println(userInput);
        }
        out.close();
        in.close();
        echoSocket.close();
    }
}