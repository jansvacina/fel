package robot;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
/**
 * Created by jan on 21-Dec-16.
 */
public class Robot {

    private static OutputStream out;


    private static DatagramSocket socket = null;
    private static DatagramPacket packet;
    private static InetAddress address;
    private static int port = 4000; //4000

    private static int connectionId = 0;

    private static final byte[]          intitMessage = new byte[]{0,0,0,0,0,0,0,0,4,1};
    private static byte[]          dataMessage = new byte[]{};
    private static final byte[]          finMessage = new byte[]{};
    private static final byte[]          rstMessage = new byte[]{};

    private static int seqInit = 0;
    private static int ackInit = 0;
    private static int flagInit = 0;

    private static boolean canContinueToData = false;
    private static int expectingSequenceNumber = 0;
    private static HashMap<Integer, byte[]> buffer = new HashMap<Integer, byte[]>();

    private static byte[] currentData = null;
    private static int currSequenceNumber = 0;
    private static final int overflowConst = 65790; // 65790;
    private static byte[] toBeAskedAckNumber = new byte[]{0,0};

    private static int sizeArray = 0;


    private static int finSeq;
    private static int finLenght;

    private static String givenAddress = "";

    private static byte[] id = null;
    public static void main(String[] args) {
        givenAddress = args[0];
        try {
            out = new FileOutputStream("foto.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            processInitPacket();
            processDataRequestPacket();
        } catch (Exception e){
            System.err.println("IOException " + e);
        }
        exit("end of data transfer");
        try {
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
    PROCESS METHODS and its sub-methods
    */
    private static void processInitPacket(){
        int loop = 0;
        while (loop < 20){

            try {
                socket = new DatagramSocket();
                socket.setSoTimeout(10000000);
                address = InetAddress.getByName(givenAddress);
//                address = InetAddress.getByName("baryk-ng.felk.cvut.cz");
//                address = InetAddress.getByName("192.168.56.101");

            } catch (Exception e){
                System.err.println("IOException " + e);
            }

            byte bff[]=new byte[264];
            packet = new DatagramPacket(intitMessage, intitMessage.length,
                    address, port);
            try {
                socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            DatagramPacket recv=
                    new DatagramPacket(bff,bff.length);
            try{
                socket.receive(recv);
                if(checkInitPacket(recv.getData())){
                    echoRecvPacket(recv.getData());
                    currentData = recv.getData();
                    id = new byte[]{recv.getData()[0], recv.getData()[1], recv.getData()[2], recv.getData()[3]};
                    String identifier = Integer.toHexString(ByteBuffer.wrap(new byte[]{recv.getData()[0], recv.getData()[1], recv.getData()[2], recv.getData()[3]}).getInt());
                    System.out.println(identifier);
                    break;
                } else{
                    socket.close();
                    exit("SOCKET closed - repeatedly invalid init packet");
                }
            } catch (SocketTimeoutException e){
                loop++;
                if(loop == 20){
                    exit("bad init packet");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private static void processDataRequestPacket(){
        byte[] receivedData = new byte[264];
        while (!isFinPacket(receivedData[8])){
            if(receivedData[8] == 1){
                exit("RST packet received");
            }
            sendDataRequest();
            receivedData = receiveDataPacket();
            if(socket.isClosed()){
                break;
            }
            sizeArray = receivedData.length-9;
            System.out.println(sizeArray);

            while (!checkDataPackage(receivedData)){
                processDataBffImg(receivedData);
                sendDataRequest();
                receivedData = receiveDataPacket();
                if(socket.isClosed()){
                    break;
                }
                sizeArray = receivedData.length-9;
            }
            savePacketToImg(receivedData);
            findFollowPacketsInBuffer();
            setToBeAskedAckNumber();
        }
    }

    private static void findFollowPacketsInBuffer(){
        int toFindSequenceNumber = currSequenceNumber+sizeArray;
        if(toFindSequenceNumber==overflowConst){
            toFindSequenceNumber -= 65536;
        }
        while(buffer.containsKey(toFindSequenceNumber)){
            savePacketToImg(buffer.get(toFindSequenceNumber));

            System.out.println("from buffer: " + toFindSequenceNumber);

            if(finSeq == convertBytes(buffer.get(toFindSequenceNumber)[4],buffer.get(toFindSequenceNumber)[5])){
                buffer.remove(toFindSequenceNumber);
                toFindSequenceNumber=toFindSequenceNumber + finLenght;
            } else {
                buffer.remove(toFindSequenceNumber);
                toFindSequenceNumber=toFindSequenceNumber+sizeArray;
            }

            if(toFindSequenceNumber==overflowConst){
                toFindSequenceNumber -= 65536;
            }
        }
        currSequenceNumber = toFindSequenceNumber;
    }

    private static void savePacketToImg(byte[] data){
        try {
            out.write(Arrays.copyOfRange(data, 9, data.length));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void processDataBffImg(byte[] data){
        int key = convertBytes(data[4],data[5]);
        if(key>currSequenceNumber){
            buffer.put(key,data);
        }

    }
    private static void setToBeAskedAckNumber(){
        if(currSequenceNumber==overflowConst){
            currSequenceNumber -= 65536;
        }
        toBeAskedAckNumber[1] = (byte) ((currSequenceNumber) % 256);
        toBeAskedAckNumber[0] = (byte) ((currSequenceNumber >> 8) % 256);

    }
    private static boolean isFinPacket(byte flag){
        if(flag == 2){
            return true;
        }
        return false;
    }
    private static void sendFinalPacket(){
        byte[] finMessage = new byte[]{id[0],id[1],id[2],id[3],0,0,toBeAskedAckNumber[0],toBeAskedAckNumber[1],2};
        DatagramPacket packet = new DatagramPacket(finMessage, finMessage.length,
                address, port);
        System.out.println("Sending FIN packet");
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        echoSend();
    }

    private static boolean checkDataPackage(byte[] recv){
        if(convertBytes(recv[4],recv[5]) != currSequenceNumber ){
            return false;
        }
        return true;
    }

    private static void sendDataRequest(){
        dataMessage = new byte[]{id[0],id[1],id[2],id[3],0,0,toBeAskedAckNumber[0],toBeAskedAckNumber[1],0};
        DatagramPacket packet = new DatagramPacket(dataMessage, dataMessage.length,
                address, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        echoSend();
    }
    private static byte[] receiveDataPacket(){
        byte buff[]=new byte[264];
        DatagramPacket recv=
                new DatagramPacket(buff,buff.length);
        try {
            socket.receive(recv);
            if(isFinPacket(recv.getData()[8])){
                sendFinalPacket();
                exit("fin");
            }
            if((recv.getLength()-9)<255){
                finSeq = convertBytes(recv.getData()[4],recv.getData()[5]);
                finLenght = recv.getLength()-9;
            }

        } catch (IOException e) {
            System.err.println("Receive data packet failed.");
            e.printStackTrace();
        }
        echoRecvPacket(recv.getData());
        return recv.getData();
    }
    /*
    CHECK METHODS
    */
    private static boolean checkInitPacket(byte[] recv){
        connectionId = convertBytes(recv[0],recv[1],recv[2],recv[3]);
        seqInit = convertBytes(recv[4],recv[5]);
        if(seqInit != 0){
            return false;
        }
        ackInit = convertBytes(recv[6],recv[7]);
        if(ackInit != 0){
            return false;
        }
        flagInit = convertBytes(recv[8]);
        if(flagInit != 4){
            return false;
        }
        if(recv[9] != 1){
            return false;
        }
        if(recv[10] != 0){
            return false;
        }
        return true;
    }
    private static void exit(String message) {
        System.out.println("Connection " + connectionId + " closed due to " + message + ".");
        socket.close();
    }
    /*
    ECHO METHODS
    */
    private static void echoRecvPacket(byte[] recv){
        StringBuilder sb = new StringBuilder();
        sb.append(" RECV SEQ = ");
        sb.append(convertBytes(recv[4],recv[5]));
        sb.append(" ACK = ");
        sb.append(convertBytes(recv[6],recv[7]));

        System.out.println(sb.toString());
    }
    public static void echoSend(){
        StringBuilder sb = new StringBuilder();
        sb.append(" SEND SWQ = 0");
        sb.append(" ACK = ");
        sb.append(convertBytes(toBeAskedAckNumber[0],toBeAskedAckNumber[1]));
        System.out.println(sb.toString());
    }
    /*
    PARSE METHODS
    */
    static int convertBytes(int b5){
        String hex = Integer.toHexString(convertUnsign(b5));
        return Integer.parseInt(hex,16);
    }
    static int convertBytes(byte b5, byte b6){
        return b6 & 0xFF | (b5 & 0xFF) << 8;
    }
    static int convertBytes(int b0, int b1, int b2, int b3){
        String hex = Integer.toHexString(convertUnsign(b0));
        hex += Integer.toHexString(convertUnsign(b1));
        hex += Integer.toHexString(convertUnsign(b2));
        hex += Integer.toHexString(convertUnsign(b3));
        return Integer.parseInt(hex,16);
    }
    static int convertUnsign(int i){
        return i< 0 ? i + 256 : i;
    }
    static int convertToUnsign(int i){
        if(i > 127){
            return i - 256;
        } else {
            return i;
        }
    }

}