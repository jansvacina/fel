#include "list.hpp"


// For logic error, you can safely remove this after implementing front and back functions.
#include <stdexcept>
#include <iostream>

void push_back(list& l, double elem) {
    list::node* node = new list::node;
    node->val = elem;
    if(l.size == 0){
        l.head = node;
        l.tail = node;
        node->prev = nullptr;
        node->next = nullptr;
    } else {
        l.tail->next = node;
        node->prev = l.tail;
        l.tail = node;

        node->next = nullptr;
    }
    l.size++;
}
/*
 * Removes an element from the end of the list.
 *
 * Precondition: Should not be called on an empty list.
 * Should run in in constant time.
 */
void pop_back(list& l) {
    if(l.tail != nullptr){
        if(l.size > 1){
            l.tail = l.tail->prev;
            delete(l.tail->next);
            l.tail->next = nullptr;
            l.size--;
        } else if (l.size == 1) {
            l.head = nullptr;
            l.tail = nullptr;
            l.size--;
        }
    }
}
/*
 * Allows access to the last element of the list.
 */
double& back(list& l) {
    if (l.size > 0){
        double& v = l.tail->val;
        return v;
    } else {
        double a = 0.0;
        double* pa = &a;
        return *pa;
    }
}
/*
 * Inserts an element at the front of the list.
 */
void push_front(list& l, double elem) {
    list::node* node2 = new list::node;
    node2->val = elem;
    if(l.size == 0){
        l.head = node2;
        l.tail = node2;
        l.head->prev = nullptr;
        l.head->next = nullptr;
        l.tail->next = nullptr;
        l.tail->prev = nullptr;
    } else {
        l.head->prev = node2;
        node2->next = l.head;
        l.head = node2;
        l.head->prev = nullptr;
    }
    l.size++;
}

/*
 * Removes an element from the front of the list.
 */
void pop_front(list& l) {
    if(l.head != nullptr){
        if(l.head == l.tail){
            delete(l.head);
            l.head = nullptr;
            l.tail = nullptr;
            l.size--;
        } else {
            l.head = l.head->next;
            delete(l.head->prev);
            l.head->prev = nullptr;

            l.size--;
        }
    }
}

/*
 * Allows access to the first element of the list.
 */
double& front(list& l) {
    if (l.head != nullptr){
        double& v = l.head->val;
        return v;
    }else {
        double a = 0.0;
        double* pa = &a;
        return *pa;
    }
}
/*
 * After dispose, the list l must be empty and not hold on to any memory.
 * Over the lifetime of a list, dispose can be called any number of times.
 * Should run in linear time.
 */
void dispose(list & l) {
    while(l.tail != nullptr){
        pop_front(l);
    }
    l.size = 0;
}
/*
 * Destructively appends list r to list l, so that list r ends up empty.
 * Should run in constant time.
 */
void join(list& l, list& r) {
    if((l.head != nullptr && l.tail != nullptr) && (r.head != nullptr && r.tail != nullptr)){
        //l and r are not empty

        l.tail->next = r.head; //1
        r.head->prev = l.tail; //2

        l.tail = r.tail;
        l.tail->prev = r.tail->prev;

        r.tail = nullptr;
        r.head = nullptr;

        l.size += r.size;
        r.size = 0;
    } else if ((r.head != nullptr && r.tail != nullptr) && (l.head == nullptr && l.tail == nullptr)){
        //l is empty
        l.head = r.head;
        l.head->next = r.head->next;
        l.tail = r.tail;
        l.tail->next = r.tail->next;
        l.size = r.size;
        r.size = 0;
        r.head = nullptr;
        r.tail = nullptr;

    }
}
/*
* Reverses order of all elements of list l.
* Given list containing 1 2 3 4 2 1, the result should be a list containing
* 1 2 4 3 2 1.
*
* Should run in linear time.
*/
void reverse(list& l) {
    if((l.head != nullptr && l.tail != nullptr)&&(l.head != l.tail)){
        if(l.head != nullptr && (l.head->next->next == nullptr)){
            list::node* tmp = nullptr;
            tmp = l.head;
            l.head = l.tail;
            l.head->next = l.tail->prev;
            l.head->prev = nullptr;

            l.tail = tmp;
            l.tail->prev = tmp->next;
            l.tail->next = nullptr;
        } else {
            list::node* ptr = nullptr;
            list::node* x = nullptr;
            //head node
            ptr = l.head;
            ptr->next = l.head->next;
            ptr->prev = ptr->next;
            ptr->next = nullptr;
            ptr = ptr->prev;
            while(ptr->prev != nullptr){
                x = ptr->prev;
                ptr->prev = ptr->next;
                ptr->next = x;
                if (ptr->prev == nullptr){
                    break;
                }
                ptr = ptr->prev;
            }

            //swamp head and tail
            list::node* tmp = nullptr;
            tmp = l.tail;
            tmp->next = l.tail->next;

            l.tail = l.head;
            l.tail->prev = l.head->prev;
            l.tail->next = nullptr;

            l.head = tmp;
            l.head->next = tmp->next;
            l.head->prev = nullptr;
        }
    }
}
/*
 * Filters elements in the list so that there are no immediate duplicates.
 * Given list containing 1 1 2 2 2 3 4 2 1, the result should be a list
 * containing 1 2 3 4 2 1.
 *
 * Should run in linear time.
 */
void unique(list& l) {
    if (l.head != nullptr){
        list::node* ptr = nullptr;
        list::node* tmp = nullptr;
        ptr = l.head;
        ptr->next = l.head->next;
        tmp = l.head;
        tmp->next = l.head->next;

        while (ptr->next != nullptr){
            if(ptr->next->val ==  ptr->val){
                if((ptr->next->next == nullptr) && (ptr->next->val = ptr->val)){
                    tmp = ptr->next;
                    tmp->next = nullptr;
                    tmp->prev = ptr;
                    ptr->next = nullptr;
                    tmp->prev = nullptr;
                    delete(tmp);
                    l.tail = ptr;
                    l.tail->prev = ptr->prev;
                    l.tail->next = nullptr;
                    l.size--;
                    break;
                }
                tmp = ptr->next;
                tmp->next = ptr->next->next;
                tmp->prev = ptr;
                ptr->next = tmp->next;
                tmp->next->prev = ptr;
                tmp->next = nullptr;
                tmp->prev = nullptr;
                delete(tmp);
                l.size--;
                tmp = ptr;
                tmp->next = ptr->next;
                if(ptr->next->val !=  ptr->val){
                    ptr = ptr->next;
                    tmp = tmp->next;
                }
            }
            else {
                ptr = ptr->next;
                tmp = tmp->next;
            }
        }
    }
    if(l.size == 1){
        l.tail = l.head;
        l.tail->val = l.head->val;
        l.tail->prev = nullptr;
        l.tail->next = nullptr;
        l.head->prev = nullptr;
        l.head->next = nullptr;

    }
}
/*
 * Returns size of the list l.
 */
size_t size(const list& l) {
    return l.size;
}
/*
 * Returns true if the list l does not contain any elements.
 */
bool empty(const list& l) {
    if (l.size == 0 && l.head == nullptr){
        return true;
    } else {
        return false;
    }
}
