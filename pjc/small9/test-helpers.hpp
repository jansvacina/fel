#ifndef PJC_HELPERS_HPP
#define PJC_HELPERS_HPP

#include <iosfwd>

struct counter {
    counter() = default;
    counter(int dc, int cc, int ca, int mc, int ma, int d, int o):default_constructors(dc),
        copy_constructors(cc), copy_assignments(ca), move_constructors(mc), move_assignments(ma),
        destructors(d), other(o) {}
    int default_constructors = 0;
    int copy_constructors = 0;
    int copy_assignments = 0;
    int move_constructors = 0;
    int move_assignments = 0;
    int destructors = 0;
    int other = 0;
};

bool operator==(const counter& c1, const counter& c2);
counter operator-(counter c1, const counter& c2);
std::ostream& operator<<(std::ostream& out, const counter& cnt);

struct tracker {
    tracker();
    tracker(double val);
    tracker(const tracker&);
    tracker& operator=(const tracker&);
    tracker(tracker&&);
    tracker& operator=(tracker&&);
    ~tracker();

    double value;
    static counter cnt;
};

bool operator==(const tracker& c1, const tracker& c2);

struct ordered {
    ordered() = default;
    ordered(int sortby, size_t order):sortby(sortby), order(order) {}

    int sortby;
    size_t order;
};

bool operator==(const ordered& o1, const ordered& o2);
bool operator<(const ordered& o1, const ordered& o2);

struct thrower {
    thrower(int, bool);
    thrower(const thrower&);
    thrower& operator=(const thrower&);
    thrower(thrower&&);
    thrower& operator=(thrower&&);

    void check() const;
    int value;
    bool should_throw;
};

bool operator==(const thrower& t1, const thrower& t2);

#endif
