#ifndef PJC_LIST_HPP
#define PJC_LIST_HPP

#include <cstddef>
#include <vector>
#include <utility>
#include <algorithm>
#include <iterator>
#include <functional>
#include <tuple>


using std::size_t;

template <typename T>
class list {
private:
    struct node {
        T val;
        node* prev;
        node* next;
    };

public:

    class const_iterator {
        node* current_ptr;
        const list* o_list;
    public:
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = const T;
        using reference = const T&;
        using pointer = const T*;

        const_iterator(){}
        const_iterator(node* ptr, const list* gen){}
        const_iterator(const const_iterator& rhs){}
        const_iterator& operator=(const const_iterator& rhs) {}

        const_iterator& operator++() {}
        const_iterator operator++(int) {}

        const_iterator& operator--() {}
        const_iterator operator--(int) {}

        reference operator*() const {}
        pointer operator->() const {}

        bool operator==(const const_iterator& rhs) const {}
        bool operator!=(const const_iterator& rhs) const {}

        friend class list;
    };

    class iterator {
        node* current_ptr;
        const list* o_list;
    public:
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = T;
        using reference = T&;
        using pointer = T*;

        iterator() {}
        iterator(node* ptr, const list* gen){}
        iterator(const iterator& rhs){}
        iterator& operator=(const iterator& rhs) {}

        iterator& operator++() {}
        iterator operator++(int) {}
        iterator& operator--() {}
        iterator operator--(int) {}

        reference operator*() const {}
        pointer operator->() const {}

        operator const_iterator() const {}

        bool operator==(const iterator& rhs) const {}
        bool operator!=(const iterator& rhs) const {}

        friend class list;
    };

    list(){}
    list(const std::vector<T>& vec) {}
    ~list() {}

    void push_back(const T& elem) {}

    void pop_back() {}

    T& back() {}
    const T& back() const {}

    void push_front(const T& elem) {}

    void pop_front() {}

    T& front() {}
    const T& front() const {}

    void join(list& r) {}

    void reverse() {}

    void unique() {}
    template <typename BinPredicate>
    void unique(BinPredicate pred) {}

    size_t size() const {}

    bool empty() const {}

    void remove(const T& value) {}

    template <typename Predicate>
    void remove_if(Predicate p) {}

    void merge(list& rhs) {}

    template <typename Comparator>
    void merge(list& rhs, Comparator cmp) {}

    // Standard copy operations.
    list(const list& rhs):list() {}
    list& operator=(const list& rhs) {}

    // Move operations
    // After the move is performed, the moved from list should be assignable and destructible.
    list(list&& rhs):list() {}
    list& operator=(list&& rhs) {}

    void swap(list& rhs) {}

    // Iterator creation -- reverse iterator overloads are intentionally missing
    iterator begin() {}
    iterator end() {}
    const_iterator begin() const {}
    const_iterator end() const {}
    const_iterator cbegin() const {}
    const_iterator cend() const {}


    bool operator==(const list& rhs) const {}
    bool operator<(const list& rhs) const {}
    bool operator<=(const list& rhs) const {}
    bool operator>(const list& rhs) const {}
    bool operator>=(const list& rhs) const {}

    std::pair<list, list> split(const_iterator place) {}

    void sort() {}

    template <typename Comparator>
    void sort(Comparator cmp){}


private:
    node* head;
    node* tail;
    size_t num_elements;
};

/*
* Two lists are not equal iff any of their elements differ.
*/
template <typename T>
bool operator!=(const list<T>& lhs, const list<T>& rhs) {}

/*
* ADL customization point for std::swap.
* Should do the same as calling lhs.swap(rhs)
*/
template <typename T>
void swap(list<T>& lhs, list<T>& rhs) {}

#endif
