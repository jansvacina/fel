#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "small1.hpp"

using namespace std;

std::pair<int, int> parse_matrix(std::istream& in){
    string s;
    ostringstream os;
    os << in.rdbuf();
    s = os.str();
    istringstream str(s);
    string line;

    int rowSize = 0;
    int columnSize = 0;
    int singleColumnSize = 0;
    bool isOnlySpaces;
    int firstLine = 0;

    while (getline(str, line))
    {
        isOnlySpaces = true;
        for (char & c : line)
        {
            if(isdigit(c)){
                isOnlySpaces = false;
            }
        }

        if (!line.empty() && isOnlySpaces == false){
            rowSize++;
            std::istringstream sstr(line);
            int n;
            while(sstr >> n) {
                singleColumnSize++;
            }

            if (firstLine == 0){
                columnSize = singleColumnSize;
            } else {
                if (singleColumnSize != columnSize){
                    rowSize = 0;
                    throw std::invalid_argument("Invalid syntax.");
                }
            }

            singleColumnSize = 0;
            firstLine++;
        }
    }
    return {rowSize,columnSize};
}

void print_table(std::ostream& out, const std::vector<std::string>& vec) {
    int counter = 0;
    int maxSize = 0;
    for(std::vector<int>::size_type i = 0; i != vec.size(); i++) {
        if (vec[i].size() > maxSize) {
            maxSize = vec[i].size() + 1;
        }
    }
    string line;


    for (int i = 0; i < ((maxSize*2)+3); i++){
        line += "-";
    }
    out << line;
    out << '\n';
    for(std::vector<int>::size_type i = 0; i != vec.size(); i++) {
        counter++;
        if (counter == 2) {
            out << std::right << setw(maxSize) << vec[i];
            out << "|";
            out << '\n';
            counter = 0;
        } else {
            out << "|";
            out << std::right << setw(maxSize) << vec[i];
            out << "|";
        }
    }
    if (counter == 1){
        for (int i = 0; i < (maxSize); i++){
            out << " ";
        }
        out << '|';
        out << '\n';
    }
    out << line;
    out << '\n';
}

/*
 * Validates a string representing a single line.
 * The expected format is [true/false] [octal number] [decimal number] [hexadecimal number]
 *
 * Returns true if the first position contains true and the three numbers have equal value
 *              or if the first position contains false and the three numbers do not have equal value;
 *              else returns false
 *
 * Worth 1 point.
 */

bool validate_line(const std::string& str) {

    vector<string> list;
    std::istringstream sstr(str);
    string n;
    while(sstr >> n) {
        list.push_back(n);
    }

    //osetreni vyjimek, na bool neni bool, na string neni string, na octal je 88 atp.

    if (list.size() != 4){

        return false;
    } else {
        string strT = "true";
        string strF = "false";
        if ((list[0].compare(strT) == 0 ) || (list[0].compare(strF) == 0 )){

            string octo{list[1]};
            string decim {list[2]};
            string hexa{list[3]};

            string::size_type sz;
            int octoDec, decimDec, hexaDec;

            try{
                octoDec = stoi(octo, 0, 8);
                stringstream sstrDecim(decim);
                decimDec;
                sstrDecim >> decimDec;
                hexaDec = stoi(hexa, &sz, 16);

                if (sz != hexa.size()){
                    return false;
                }
            }catch(invalid_argument a) {
                return false;

            }

            if ((octoDec == decimDec) && (decimDec == hexaDec) && (list[0] == "true")){
                return true;
            } else if ( ( (octoDec != decimDec) || (decimDec != hexaDec) || (decimDec != octoDec) ) && (list[0] == "false")){
                return true;
            } else{
                return false;
            }
        } else {
            cout << list[0] << endl;
            return false;
        }
    }
}


/*
 * Returns string representing the largest of comma separated numbers inside a stream.
 *
 * All input numbers will be positive and will not contain leading zeroes.
 *
 * Worth 1 point.
 */

std::string max_number(std::istream& in) {

    int maxSize = 0;
    vector<string> maxStrings;
    int counter = 0;
    /*
     * Parsing istream to string to stringstream
     */
    string s;
    ostringstream os;
    os << in.rdbuf();
    s = os.str();
    stringstream sstr(s);

    /*
     * Parsing stringstream to vector and checking the longest
     */
    while( sstr.good() )
    {
        string n;
        string item;
        getline( sstr, item, ',' );
        stringstream strItem (item);
        strItem >> n;
        if (counter == 0){
            maxStrings.push_back(n);
            maxSize = n.size();
        }

        if (n.size() > maxSize){
            maxStrings.clear();
            maxSize = n.size();
            maxStrings.push_back(n);

        }

        if (n.size() == maxSize && counter != 0){
            maxStrings.push_back(n);

        }

        counter++;

    }

    if (maxStrings.size() == 1){
        return {maxStrings[0]};
    } else if (maxStrings.size() == 0){
        return {""};
    } else {
        std::stringstream ssstream(maxStrings[0]);
        char msi;

        std::vector<char> stringsItems;
        while (ssstream >> msi){
            stringsItems.push_back(msi);
        }

        vector<char> maxStringsItem = stringsItems;


        for (int i = 1; i < maxStrings.size(); i++){
            std::stringstream ssstream(maxStrings[i]);
            char msi;

            std::vector<char> stringsItems;
            while (ssstream >> msi){
                stringsItems.push_back(msi);
            }

            for (int j=0; j < stringsItems.size(); j++){

                if ( (int) stringsItems[j] > (int) maxStringsItem[j]){
                    maxStringsItem = stringsItems;
                }
            }

        }
        string toReturn;
        for (int i = 0; i < maxStringsItem.size(); i ++){
            toReturn += maxStringsItem[i];
        }
        return {toReturn};


    }


}
