#pragma once

#include <vector>

/*
 * Struct representing graph, that is, vertices and edges between the vertices.
 * Vertices are identified with indices, where 0 stands for 1st added vertex,
 * 1 stands for 2nd added vertex, 2 stands for 3rd added vertex, etc...
 *
 * The edges between vertices are directed.
 */
struct graph {
    std::vector<std::vector<int>> connections;
};

/*
 * Perform any necessary initialization and leave the graph containing the specified number of vertices.
 *
 * Can be called only once on any given graph.
 */
void init_graph(graph& g, int vertices = 0);

/*
 * Remove all vertices and edges from graph.
 */
void clear_graph(graph& g);

/*
 * Add a vertex to the graph
 */
void add_vertex(graph& g);

/*
* Return the number of vertices in graph.
*/
std::size_t num_vertices(const graph& g);

// 1 point for the functions above

/*
 * Add a directed edge between the two vertices.
 * If there already is a directed edge, does nothing.
 *
 * Precondition: Can only be called with existing vertex.
 */
void add_edge(graph& g, int from, int to);

/*
* Return all vertices we can get to from the given vertex.
*
* Can only be called with existing vertex.
*/
std::vector<int> connections(const graph& g, int vertex);

// 1 points for the 2 functions above

/*
 * Return vertices that are reachable from given vertex.
 * That is, the vertex itself,
            all vertices connected to the given vertex,
            all vertices connected to these vertices,
            etc...
 *
 * Can only be called with existing vertex.
 * 2 points
 */
std::vector<int> reachable_vertices(const graph& g, int vertex);
