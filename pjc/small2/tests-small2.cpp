#include <algorithm>

#include "catch.hpp"

#include "graph.hpp"

namespace {

bool set_equal(std::vector<int> list1, std::vector<int> list2) {
    std::sort(list1.begin(), list1.end());
    std::sort(list2.begin(), list2.end());
    return list1 == list2;
}

} // anon namespace

TEST_CASE("Basic functionality") {
    graph g;
    WHEN("Graph is inited") {
        init_graph(g);
        THEN("Graph should have no vertices") {
            REQUIRE(num_vertices(g) == 0);
        }
        AND_WHEN("Graph is cleared") {
            clear_graph(g);
            THEN("Graph should have no nodes") {
                REQUIRE(num_vertices(g) == 0);
            }
            AND_WHEN("A vertex is added") {
                add_vertex(g);
                THEN("Graph should contain a node") {
                    REQUIRE(num_vertices(g) == 1);
                }
            }
        }
        AND_WHEN("Vertices were added") {
            add_vertex(g);
            add_vertex(g);
            AND_WHEN("Graph is cleared") {
                clear_graph(g);
                THEN("Graph should have no nodes.") {
                    REQUIRE(num_vertices(g) == 0);
                }
            }
            AND_WHEN("Edges were added") {
                add_edge(g, 0, 1);
                add_edge(g, 1, 0);
                AND_WHEN("Graph is cleared") {
                    clear_graph(g);
                    THEN("Graph should be empty.") {
                        REQUIRE(num_vertices(g) == 0);
                    }
                }
            }
        }
        AND_WHEN("Vertices were added") {
            add_vertex(g);
            add_vertex(g);
            add_vertex(g);
            THEN("Graph should have the vertices") {
                REQUIRE(num_vertices(g) == 3);
                THEN("Cleared out graph should have no vertices again.") {
                    clear_graph(g);
                    REQUIRE(num_vertices(g) == 0);
                }
            }
        }
    }
    WHEN("Graph is inited with an argument") {
        init_graph(g, 10);
        THEN("Graph should have the correct number of vertices") {
            REQUIRE(num_vertices(g) == 10);
        }
    }
}


TEST_CASE("connections, add_edge") {
    graph g;
    WHEN("Graph is initialized") {
        init_graph(g, 3);
        AND_WHEN("No edges were added") {
            THEN("The connection from any edge should be empty.") {
                for (int i = 0; i < 3; ++i) {
                    REQUIRE(connections(g, i).empty());
                }
            }
        }
        AND_WHEN("We add an edge to the graph") {
            add_edge(g, 0, 1);
            THEN("The there should be a connection from given edge") {
                REQUIRE(connections(g, 0).at(0) == 1);
            }
            AND_WHEN("We add another edge to the graph") {
                add_edge(g, 0, 2);
                THEN("Both edges should be returned as a connection") {
                    REQUIRE(connections(g, 0).size() == 2);
                    REQUIRE(set_equal(connections(g, 0), { 1, 2 }));
                }
            }
        }
        AND_WHEN("We add an edge multiple times") {
            add_edge(g, 0, 1);
            add_edge(g, 0, 1);
            add_edge(g, 0, 1);
            THEN("Only one should be added") {
                REQUIRE(connections(g, 0).size() == 1);
                REQUIRE(connections(g, 0).at(0) == 1);
            }
            AND_WHEN("We add another edge") {
                add_edge(g, 0, 2);
                add_edge(g, 0, 2);
                THEN("There should be edges to both the edges.") {
                    REQUIRE(connections(g, 0).size() == 2);
                    REQUIRE(set_equal(connections(g, 0), { 1, 2 }));
                }
            }
        }
    }
}


TEST_CASE("Reachable vertices") {
    graph g;
    
    SECTION("Simple graph") {
        init_graph(g, 4);
    
        add_edge(g, 0, 1);
        add_edge(g, 1, 2);
        add_edge(g, 1, 3);
    
        REQUIRE(set_equal(reachable_vertices(g, 0), { 0, 1, 2, 3 }));
        REQUIRE(set_equal(reachable_vertices(g, 1), { 1, 2, 3 }));
        REQUIRE(set_equal(reachable_vertices(g, 2), { 2 }));
        REQUIRE(set_equal(reachable_vertices(g, 3), { 3 }));
    }

    SECTION("Complex graph") {
        init_graph(g, 13);

        add_edge(g, 0, 1);
        add_edge(g, 1, 2);
        add_edge(g, 2, 0);

        add_edge(g, 3, 4);
        add_edge(g, 4, 5);
        add_edge(g, 5, 6);
        add_edge(g, 6, 3);

        add_edge(g, 7, 8);
        add_edge(g, 8, 7);

        add_edge(g, 6, 2);
        add_edge(g, 7, 4);
        add_edge(g, 4, 9);

        add_edge(g, 10, 11);
        add_edge(g, 11, 12);

        REQUIRE(set_equal(reachable_vertices(g, 0), { 0, 1, 2 }));
        REQUIRE(set_equal(reachable_vertices(g, 1), { 0, 1, 2 }));
        REQUIRE(set_equal(reachable_vertices(g, 2), { 0, 1, 2 }));
        REQUIRE(set_equal(reachable_vertices(g, 3), { 0, 1, 2, 3, 4, 5, 6, 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 4), { 0, 1, 2, 3, 4, 5, 6, 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 5), { 0, 1, 2, 3, 4, 5, 6, 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 6), { 0, 1, 2, 3, 4, 5, 6, 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 7), { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 8), { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 9), { 9 }));
        REQUIRE(set_equal(reachable_vertices(g, 10), { 10, 11, 12 }));
        REQUIRE(set_equal(reachable_vertices(g, 11), { 11, 12 }));
        REQUIRE(set_equal(reachable_vertices(g, 12), { 12 }));
    }
}


TEST_CASE("Const verifiers") {
    // This test serves only to verify that the selected functions have proper
    // api vis-a-vis const correctness.
    graph g;
    init_graph(g);
    add_vertex(g);
    const graph& cg = g;
    num_vertices(cg);
    connections(cg, 0);
    reachable_vertices(cg, 0);
    REQUIRE(true);
}
