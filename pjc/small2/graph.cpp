#include <iostream>
#include "graph.hpp"

using namespace std;
/*
* Perform any necessary initialization and leave the graph containing the specified number of vertices.
*
* Can be called only once on any given graph.
*/
void init_graph(graph& g, int vertices) {
    if (vertices != 0){
        for (int i=0; i < vertices; i++){
            std:: vector<int> v;
            g.connections.push_back(v);
        }
    }
}
/*
 * Remove all vertices and edges from graph.
 */
void clear_graph(graph& g) {
    g.connections.clear();
}
/*
 * Add a vertex to the graph
 */
void add_vertex(graph& g) {
    std:: vector<int> v;
    g.connections.push_back(v);
}

/*
* Return the number of vertices in graph.
*/
std::size_t num_vertices(const graph& g) {
    return g.connections.size();
}
/*
 * Add a directed edge between the two vertices.
 * If there already is a directed edge, does nothing.
 *
 * Precondition: Can only be called with existing vertex.
 *
 * from do ktereho to ulozim, na jaky index, to jakou hodnotu do toho ulozim + chceck te precondition
 */
void add_edge(graph& g, int from, int to) {
    bool contain = false;
    if(!(g.connections[from].empty())){
        int size = static_cast<int>(g.connections[from].size());
        for (int i = 0; i < size; i++){
            if(to == g.connections[from][i]){
                contain = true;
                break;
            }
        }
        if(contain == false){
            g.connections[from].push_back(to);
        }
    } else{g.connections[from].push_back(to);}

}

/*
 * Return vertices that are reachable from given vertex.
 * That is, the vertex itself,
            all vertices connected to the given vertex,
            all vertices connected to these vertices,
            etc...
 *
 * Can only be called with existing vertex.
 * 2 points
 */

std::vector<int> reachable_vertices(const graph& g, int vertex) {
    std::vector<int> visited;
    vector<int> toBeVisited;
    bool exists = false;
    toBeVisited.push_back(vertex);
    while(!(toBeVisited.empty())){
        visited.push_back(toBeVisited[0]);
        vector<int> vert = g.connections[toBeVisited[0]];
        int vertSize = static_cast<int>(vert.size());
        if(vertSize > 0){
            for (int i = 0; i < vertSize; i++){
                int visitedSize = static_cast<int>(visited.size());
                for (int j = 0; j < visitedSize; j++){
                    if(visited[j] == vert[i]){
                        exists = true;
                    }
                }
                if (exists==false){
                    toBeVisited.push_back(vert[i]);
                }
                exists = false;
            }
        }
        toBeVisited.erase(toBeVisited.begin());
    }
    return {visited};
}
 std::vector<int> connections(const graph& g, int vertex) {
    return {g.connections[vertex]};
}
