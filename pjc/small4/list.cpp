#include "list.hpp"


// For logic error, you can safely remove this after implementing front and back functions.
#include <stdexcept>

/*
* After constructor the list must be usable.
*/
list::list()
{
    head = nullptr;
    tail = nullptr;
    num_elements = 0;
}

/*
* Creates a list containing all elements from vec.
*/
list::list(const std::vector<double>& vec) {
    head = nullptr;
    tail = nullptr;
    num_elements = 0;


    for (int i = 0; i < static_cast<int>(vec.size()); i++)
    {
        push_back(vec[i]);
    }
}



list::~list() {
    while(!empty()){
        pop_back();
    }
}

void list::push_back(double elem) {
    list::node* node = new list::node;
    node->prev = nullptr;
    node->next = nullptr;
    node->val = 0;
    node->val = elem;
    if (this->head == nullptr) {
        this->head = node;
        this->tail = node;

    }
    else {
        this->tail->next = node;
        node->prev = this->tail;
        this->tail = node;
    }
    this->num_elements++;

}

void list::pop_back() {
    if (this->tail != nullptr) {
        if (this->num_elements > 1) {
            this->tail = this->tail->prev;
            delete this->tail->next; //delete je operator
            this->tail->next = nullptr;
            this->num_elements--;
        }
        else if (this->num_elements == 1) {
            this->head = nullptr;
            this->tail = nullptr;
            this->num_elements--;
        }
    }
}

double& list::back() {
    if (this->num_elements > 0) {
        double& v = this->tail->val;
        return v;
    }
    else {
        double a = 0.0;
        double* pa = &a;
        return *pa;
    }
}

double list::back() const {
    if (this->num_elements > 0) {
        double v = this->tail->val;
        return v;
    }
    else {
        return 0.0;
    }
}

void list::push_front(double elem) {
    list::node* node2 = new list::node;
    node2->val = elem;
    if (this->num_elements == 0) {
        this->head = node2;
        this->tail = node2;
        this->head->prev = nullptr;
        this->head->next = nullptr;
        this->tail->next = nullptr;
        this->tail->prev = nullptr;
    }
    else {
        this->head->prev = node2;
        node2->next = this->head;
        this->head = node2;
        this->head->prev = nullptr;
    }
    this->num_elements++;
}

void list::pop_front() {
    if (this->head != nullptr) {
        if (this->head == this->tail) {
            delete(this->head);
            this->head = nullptr;
            this->tail = nullptr;
            this->num_elements--;
        }
        else {
            this->head = this->head->next;
            delete(this->head->prev);
            this->head->prev = nullptr;

            this->num_elements--;
        }
    }
}

double& list::front() {
    if (this->head != nullptr) {
        double& v = this->head->val;
        return v;
    }
    else {
        double a = 0.0;
        double* pa = &a;
        return *pa;
    }
}

double list::front() const {
    if (this->head != nullptr) {
        double v = this->head->val;
        return v;
    }
    else {
        return 0.0;
    }
}

void list::join(list& r) {
    if (this->empty()) {

    }
    if((this->head != nullptr && this->tail != nullptr) && (r.head != nullptr && r.tail != nullptr)){
        //l and r are not empty

        this->tail->next = r.head; //1
        r.head->prev = this->tail; //2

        this->tail = r.tail;
        this->tail->prev = r.tail->prev;

        r.tail = nullptr;
        r.head = nullptr;

        this->num_elements += r.num_elements;
        r.num_elements = 0;
    } else if ((r.head != nullptr && r.tail != nullptr) && (this->head == nullptr && this->tail == nullptr)){
        //l is empty
        this->head = r.head;
        this->head->next = r.head->next;
        this->tail = r.tail;
        this->tail->next = r.tail->next;
        this->num_elements = r.num_elements;
        r.num_elements = 0;
        r.head = nullptr;
        r.tail = nullptr;

    }
    if(r.head != nullptr){
        list::node* tmp = r.head;
        for (int i = 0; i < static_cast<int>(r.num_elements) ; i ++){
            push_back(tmp->val);
            if(tmp->next != nullptr){
                tmp = tmp->next;
            }
            if(tmp->prev != nullptr){
                delete(tmp->prev);
            }

        }

    }
}

void list::reverse() {
    if ((this->head != nullptr && this->tail != nullptr) && (this->head != this->tail)) {
        if (this->head != nullptr && (this->head->next->next == nullptr)) {
            list::node* tmp = nullptr;
            tmp = this->head;
            this->head = this->tail;
            this->head->next = this->tail->prev;
            this->head->prev = nullptr;

            this->tail = tmp;
            this->tail->prev = tmp->next;
            this->tail->next = nullptr;
        }
        else {
            list::node* ptr = nullptr;
            list::node* x = nullptr;
            //head node
            ptr = this->head;
            ptr->next = this->head->next;
            ptr->prev = ptr->next;
            ptr->next = nullptr;
            ptr = ptr->prev;
            while (ptr->prev != nullptr) {
                x = ptr->prev;
                ptr->prev = ptr->next;
                ptr->next = x;
                if (ptr->prev == nullptr) {
                    break;
                }
                ptr = ptr->prev;
            }

            //swamp head and tail
            list::node* tmp = nullptr;
            tmp = this->tail;
            tmp->next = this->tail->next;

            this->tail = this->head;
            this->tail->prev = this->head->prev;
            this->tail->next = nullptr;

            this->head = tmp;
            this->head->next = tmp->next;
            this->head->prev = nullptr;
        }
    }
}

void list::unique() {
    if (this->head != nullptr) {
        list::node* ptr = nullptr;
        list::node* tmp = nullptr;
        ptr = this->head;
        ptr->next = this->head->next;
        tmp = this->head;
        tmp->next = this->head->next;

        while (ptr->next != nullptr) {
            if (ptr->next->val == ptr->val) {
                if ((ptr->next->next == nullptr) && (ptr->next->val == ptr->val)) {
                    tmp = ptr->next;
                    tmp->next = nullptr;
                    tmp->prev = ptr;
                    ptr->next = nullptr;
                    tmp->prev = nullptr;
                    delete(tmp);
                    this->tail = ptr;
                    this->tail->prev = ptr->prev;
                    this->tail->next = nullptr;
                    this->num_elements--;
                    break;
                }
                tmp = ptr->next;
                tmp->next = ptr->next->next;
                tmp->prev = ptr;
                ptr->next = tmp->next;
                tmp->next->prev = ptr;
                tmp->next = nullptr;
                tmp->prev = nullptr;
                delete(tmp);
                this->num_elements--;
                tmp = ptr;
                tmp->next = ptr->next;
                if (ptr->next->val != ptr->val) {
                    ptr = ptr->next;
                    tmp = tmp->next;
                }
            }
            else {
                ptr = ptr->next;
                tmp = tmp->next;
            }
        }
    }
    if (this->num_elements == 1) {
        this->tail = this->head;
        this->tail->val = this->head->val;
        this->tail->prev = nullptr;
        this->tail->next = nullptr;
        this->head->prev = nullptr;
        this->head->next = nullptr;

    }
}

std::size_t list::size() const {
    return this->num_elements;
}

bool list::empty() const
{
    if (this->head == nullptr) {
        return true;
    }
    else {
        return false;
    }
}
/*
* Removes all elements with the specified value from the list.
*
* Should run in time linear to size of the list.
*/
void list::remove(double value) {

    list::node* ptr = head;
    list::node* tmp = head;

    while (tmp!=nullptr){
        if (ptr->val == value) {

            tmp = tmp->next;//dalsi
            if (ptr == head){
                head = tmp;
            }
            else{
                ptr->prev->next = ptr->next;
            }

            if (ptr == tail){
                tail = ptr->prev;

            }
            else {
                ptr->next->prev = ptr->prev;
            }

            delete ptr;
            ptr = tmp;
            num_elements--;
        }
        else {
            ptr = ptr->next;
            tmp = tmp->next;
        }

    }
}

void list::merge(list& rhs) {

    //1. rhs is empty
    if (rhs.empty()){
        return;
    }

    //2. lhs is empty
    if (empty()){
        join(rhs);
        return;
    }

    //3. both non empty
    list::node* lp = head;
    list::node* rp = rhs.head;
    list::node* rx = rp->next;

    while (!(rp == nullptr && lp->next == nullptr)) {

        //3.1. reached the end of rhs
        if (rp == nullptr){
            return;
        }

        //3.2 l is null pointer
        if(lp->next == nullptr){
            join(rhs);
            return;
        }

        //3.3 r value is equal or greater than
        if(rp->val > lp->val){
            lp = lp->next;
        } else {
            //#1
            list::node* x = rp->next;

            //priradit ten prvek rhs -> lhs
            if (lp->prev != nullptr){
                //lp prev s rp
                lp->prev->next = rp;
                rp->prev = lp->prev;

                //rp next s lp
                lp->prev = rp;
                rp->next = lp;
            }else{
                head = rp;
                rp->prev = nullptr;

                rp->next = lp;
                lp->prev = rp;
            }
            rp = x;
            rhs.head = rp;
            num_elements++;
            rhs.num_elements--;
        }
    }
}
