#ifndef PJC_LIST_HPP
#define PJC_LIST_HPP

#include <cstddef>
#include <vector>
#include <utility>
#include <algorithm>
#include <iterator>
#include <functional>
#include <tuple>
#include "test-helpers.hpp"


using std::size_t;

template <typename T>
class list {
private:
    struct node {
        T val;
        node* prev;
        node* next;
    };

public:

    class const_iterator {
        node* current_ptr;
        const list* o_list;
    public:
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = const T;
        using reference = const T&;
        using pointer = const T*;

        const_iterator(){
            current_ptr = nullptr;
            o_list = nullptr;
        }
        const_iterator(node* ptr, const list* gen){
            current_ptr = ptr;
            o_list = gen;
        }
        const_iterator(const const_iterator& rhs){
            current_ptr = rhs.current_ptr;
            o_list = rhs.o_list;
        }
        const_iterator& operator=(const const_iterator& rhs) {
            current_ptr = rhs.current_ptr;
            o_list = rhs.o_list;
            return *this;
        }

        const_iterator& operator++() {
            if(current_ptr->next != nullptr){
                current_ptr = current_ptr->next;
            } else if(current_ptr->next == nullptr){
                current_ptr = nullptr;
                return *this;
            } else {
                return *this;
            }
            return *this;
        }
        const_iterator operator++(int) {
            list::const_iterator copy(*this);
            ++(*this);
            return copy;
        }

        const_iterator& operator--() {
            if(current_ptr == nullptr) {
                current_ptr = o_list->tail;
                return *this;
            } else if(current_ptr->prev == nullptr){
                return *this;
            } else if (current_ptr != nullptr && current_ptr->prev != nullptr){
                current_ptr = current_ptr->prev;
                return *this;
            } else {
                return *this;
            }
        }
        const_iterator operator--(int) {
            list::const_iterator copy(*this);
            --(*this);
            return copy;
        }

        reference operator*() const {
            return current_ptr->val;
        }
        pointer operator->() const {
            return &current_ptr->val;
        }

        bool operator==(const const_iterator& rhs) const {
            return current_ptr == rhs.current_ptr;
        }
        bool operator!=(const const_iterator& rhs) const {
            return current_ptr != rhs.current_ptr;
        }

        friend class list;
    };

    class iterator {
        node* current_ptr;
        const list* o_list;
    public:
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = T;
        using reference = T&;
        using pointer = T*;

        iterator() {
            current_ptr = nullptr;
            o_list = nullptr;
        }
        iterator(node* ptr, const list* gen){
            current_ptr = ptr;
            o_list = gen;
        }
        iterator(const iterator& rhs){
            current_ptr = rhs.current_ptr;
            o_list = rhs.o_list;
        }
        iterator& operator=(const iterator& rhs) {
            current_ptr = rhs.current_ptr;
            o_list = rhs.o_list;
            return *this;
        }

        iterator& operator++() {
            if(current_ptr->next != nullptr){
                current_ptr = current_ptr->next;
            } else if(current_ptr->next == nullptr){
                current_ptr = nullptr;
                return *this;
            } else {
                return *this;
            }
            return *this;
        }
        iterator operator++(int) {
            list::iterator copy(*this);
            ++(*this);
            return copy;
        }
        iterator& operator--() {
            if(current_ptr == nullptr) {
                current_ptr = o_list->tail;
                return *this;
            } else if(current_ptr->prev == nullptr){
                return *this;
            } else if (current_ptr != nullptr && current_ptr->prev != nullptr){
                current_ptr = current_ptr->prev;
                return *this;
            } else {
                return *this;
            }
        }
        iterator operator--(int) {
            list::iterator copy(*this);
            --(*this);
            return copy;
        }

        reference operator*() const {
            return current_ptr->val;
        }
        pointer operator->() const {
            return &current_ptr->val;
        }

        operator const_iterator() const {
            return list::const_iterator(current_ptr, o_list);
        }

        bool operator==(const iterator& rhs) const {
            return current_ptr == rhs.current_ptr;
        }
        bool operator!=(const iterator& rhs) const {
            return current_ptr != rhs.current_ptr;
        }

        friend class list;
    };

    list(){
        head = nullptr;
        tail = nullptr;
        num_elements = 0;
    }
    list(const std::vector<T>& vec) {
        head = nullptr;
        tail = nullptr;
        num_elements = 0;

        for (int i = 0; i < static_cast<int>(vec.size()); i++){
            push_back(vec[i]);
        }
    }
    ~list() {
        while(!empty()){
            pop_front();
        }

        head = nullptr;
        tail = nullptr;
        num_elements = 0;
    }

    void push_back(const T& elem) {
        list::node* node = new list::node;
        node->prev = nullptr;
        node->next = nullptr;
        node->val = elem;
        if (this->head == nullptr) {
            this->head = node;
            this->tail = node;

        }
        else {
            this->tail->next = node;
            node->prev = this->tail;
            this->tail = node;
        }
        this->num_elements++;
    }

    void pop_back() {
        if (this->tail != nullptr) {
            if (this->num_elements > 1) {
                this->tail = this->tail->prev;
                delete this->tail->next; //dle J. Brabce je delete jen operator
                this->tail->next = nullptr;
                this->num_elements--;
            }
            else if (this->num_elements == 1) {
                delete this->head;
                this->head = nullptr;
                this->tail = nullptr;
                this->num_elements--;
            }

        }
    }

    T& back() {
        if (this->num_elements > 0) {
            T& v = this->tail->val;
            return v;
        }
        else {
//            T& a = nullptr;
            return this->tail->val;
        }
    }
    const T& back() const {
        if (this->num_elements > 0) {
//            double v = this->tail->val;
            return this->tail->val;
        }
        else {
            return this->tail->val;
        }
    }

    void push_front(const T& elem) {
        list::node* node2 = new list::node;
        node2->val = elem;
        if (this->num_elements == 0) {
            this->head = node2;
            this->tail = node2;
            this->head->prev = nullptr;
            this->head->next = nullptr;
            this->tail->next = nullptr;
            this->tail->prev = nullptr;
        }
        else {
            this->head->prev = node2;
            node2->next = this->head;
            this->head = node2;
            this->head->prev = nullptr;
        }
        this->num_elements++;
    }

    void pop_front() {
        if (this->head != nullptr) {
            if (this->head == this->tail) {
                delete(this->head);
                this->head = nullptr;
                this->tail = nullptr;
                this->num_elements--;
            }
            else {
                this->head = this->head->next;
                delete(this->head->prev);
                this->head->prev = nullptr;

                this->num_elements--;
            }
        }
    }

    T& front() {
        if (this->head != nullptr) {
            T& v = this->head->val;
            return v;
        }
        else {

            return this->head->val;
        }
    }
    const T& front() const {
        if (this->head != nullptr) {
            T& v = this->head->val;
            return v;
        }
        else {
            return this->head->val;
        }
    }

    void join(list& r) {
        if (this->empty()) {

        }
        if((this->head != nullptr && this->tail != nullptr) && (r.head != nullptr && r.tail != nullptr)){
            //l and r are not empty

            this->tail->next = r.head; //1
            r.head->prev = this->tail; //2

            this->tail = r.tail;
            this->tail->prev = r.tail->prev;

            r.tail = nullptr;
            r.head = nullptr;

            this->num_elements += r.num_elements;
            r.num_elements = 0;
        } else if ((r.head != nullptr && r.tail != nullptr) && (this->head == nullptr && this->tail == nullptr)){
            //l is empty
            this->head = r.head;
            this->head->next = r.head->next;
            this->tail = r.tail;
            this->tail->next = r.tail->next;
            this->num_elements = r.num_elements;
            r.num_elements = 0;
            r.head = nullptr;
            r.tail = nullptr;

        }
        if(r.head != nullptr){
            list::node* tmp = r.head;
            for (int i = 0; i < static_cast<int>(r.num_elements) ; i ++){
                push_back(tmp->val);
                if(tmp->next != nullptr){
                    tmp = tmp->next;
                }
                if(tmp->prev != nullptr){
                    delete(tmp->prev);
                }

            }

        }
    }

    void reverse() {
        if ((this->head != nullptr && this->tail != nullptr) && (this->head != this->tail)) {
            if (this->head != nullptr && (this->head->next->next == nullptr)) {
                list::node* tmp = nullptr;
                tmp = this->head;
                this->head = this->tail;
                this->head->next = this->tail->prev;
                this->head->prev = nullptr;

                this->tail = tmp;
                this->tail->prev = tmp->next;
                this->tail->next = nullptr;
            }
            else {
                list::node* ptr = nullptr;
                list::node* x = nullptr;
                //head node
                ptr = this->head;
                ptr->next = this->head->next;
                ptr->prev = ptr->next;
                ptr->next = nullptr;
                ptr = ptr->prev;
                while (ptr->prev != nullptr) {
                    x = ptr->prev;
                    ptr->prev = ptr->next;
                    ptr->next = x;
                    if (ptr->prev == nullptr) {
                        break;
                    }
                    ptr = ptr->prev;
                }

                //swamp head and tail
                list::node* tmp = nullptr;
                tmp = this->tail;
                tmp->next = this->tail->next;

                this->tail = this->head;
                this->tail->prev = this->head->prev;
                this->tail->next = nullptr;

                this->head = tmp;
                this->head->next = tmp->next;
                this->head->prev = nullptr;
            }
        }
    }

    void unique() {
        auto pred = [](const auto& lhs, const auto& rhs) {
            return lhs == rhs;
        };
        unique(pred);
    }
    template <typename BinPredicate>
    void unique(BinPredicate pred) {
        if (this->head != nullptr) {
            list::node* ptr = nullptr;
            list::node* tmp = nullptr;
            ptr = this->head;
            ptr->next = this->head->next;
            tmp = this->head;
            tmp->next = this->head->next;

            while (ptr->next != nullptr) {

                if (pred(ptr->next->val,ptr->val)) {
                    if ((ptr->next->next == nullptr) && (pred(ptr->next->val, ptr->val))) {
                        tmp = ptr->next;
                        tmp->next = nullptr;
                        tmp->prev = ptr;
                        ptr->next = nullptr;
                        tmp->prev = nullptr;
                        delete(tmp);
                        this->tail = ptr;
                        this->tail->prev = ptr->prev;
                        this->tail->next = nullptr;
                        this->num_elements--;
                        break;
                    }
                    tmp = ptr->next;
                    tmp->next = ptr->next->next;
                    tmp->prev = ptr;
                    ptr->next = tmp->next;
                    tmp->next->prev = ptr;
                    tmp->next = nullptr;
                    tmp->prev = nullptr;
                    delete(tmp);
                    this->num_elements--;
                    tmp = ptr;
                    tmp->next = ptr->next;
                    if (!pred(ptr->next->val, ptr->val)) {
                        ptr = ptr->next;
                        tmp = tmp->next;
                    }
                }
                else {
                    ptr = ptr->next;
                    tmp = tmp->next;
                }
            }
        }
        if (this->num_elements == 1) {
            this->tail = this->head;
            this->tail->val = this->head->val;
            this->tail->prev = nullptr;
            this->tail->next = nullptr;
            this->head->prev = nullptr;
            this->head->next = nullptr;

        }
    }

    size_t size() const {
        return this->num_elements;
    }

    bool empty() const {
        if (this->head == nullptr) {
            return true;
        }
        else {
            return false;
        }
    }

    void remove(const T& value) {
        list::node* ptr = head;
        list::node* tmp = head;

        while (tmp!=nullptr){
            if (ptr->val == value) {

                tmp = tmp->next;//dalsi
                if (ptr == head){
                    head = tmp;
                }
                else{
                    ptr->prev->next = ptr->next;
                }

                if (ptr == tail){
                    tail = ptr->prev;

                }
                else {
                    ptr->next->prev = ptr->prev;
                }

                delete ptr;
                ptr = tmp;
                num_elements--;
            }
            else {
                ptr = ptr->next;
                tmp = tmp->next;
            }

        }
    }

    template <typename Predicate>
    void remove_if(Predicate p) {
        list::node* ptr = head;
        list::node* tmp = head;

        while (tmp!=nullptr){
            if (p(ptr->val)) {

                tmp = tmp->next;//dalsi
                if (ptr == head){
                    head = tmp;
                }
                else{
                    ptr->prev->next = ptr->next;
                }

                if (ptr == tail){
                    tail = ptr->prev;

                }
                else {
                    ptr->next->prev = ptr->prev;
                }

                delete ptr;
                ptr = tmp;
                num_elements--;
            }
            else {
                ptr = ptr->next;
                tmp = tmp->next;
            }

        }
    }

    void merge(list& rhs) {
        auto pred = [](const auto& lhs, const auto& rhs) {
            return lhs < rhs;
        };
        merge(rhs, pred);
    }

    template <typename Comparator>
    void merge(list& rhs, Comparator cmp) {
        //1. rhs is empty
        if (rhs.empty()){
            return;
        }

        //2. lhs is empty
        if (empty()){
            join(rhs);
            return;
        }

        //2.a both one


        //3. both non empty
        list::node* lp = head;
        list::node* rp = rhs.head;

        while (!(rp == nullptr && lp->next == nullptr)) {

            //3.1. reached the end of rhs
            if (rp == nullptr){
                return;
            }

            //3.2 l is null pointer
            if(lp == nullptr){
                join(rhs);
                return;
            }

            //3.3 r value is equal or greater than
            if(cmp(rp->val, lp->val)){
                lp = lp->next;
            } else {
                //#1
                list::node* x = rp->next;

                //priradit ten prvek rhs -> lhs
                if (lp->prev != nullptr){
                    //lp prev s rp
                    lp->prev->next = rp;
                    rp->prev = lp->prev;

                    //rp next s lp
                    lp->prev = rp;
                    rp->next = lp;
                }else{
                    head = rp;
                    rp->prev = nullptr;

                    rp->next = lp;
                    lp->prev = rp;
                }
                rp = x;
                rhs.head = rp;
                num_elements++;
                rhs.num_elements--;
            }
        }
    }

    // Standard copy operations.
    list(const list& rhs):list() {
        head = nullptr;
        tail = nullptr;
        num_elements = 0;

        list::const_iterator ptr = rhs.begin();
        while(ptr != rhs.end()){
            push_back(ptr.current_ptr->val);
            ++ptr;
        }
    }
    list& operator=(const list& rhs) {
        list copy(rhs);
        copy.swap(*this);
        return (*this);
    }

    // Move operations
    // After the move is performed, the moved from list should be assignable and destructible.
    list(list&& rhs):list() {
        rhs.swap(*this);
    }
    list& operator=(list&& rhs) {
        std::swap(head, rhs.head);
        std::swap(tail, rhs.tail);
        std::swap(num_elements, rhs.num_elements);

        return (*this);
    }

    void swap(list& rhs) {
        std::swap(head, rhs.head);
        std::swap(tail, rhs.tail);
        std::swap(num_elements, rhs.num_elements);
    }

    // Iterator creation -- reverse iterator overloads are intentionally missing
    iterator begin() {
        return{head, this};
    }
    iterator end() {
        return {nullptr,this};
    }
    const_iterator begin() const {
        return{head, this};
    }
    const_iterator end() const {
        return {nullptr,this};
    }
    const_iterator cbegin() const {
        return{head, this};
    }
    const_iterator cend() const {
        return {nullptr,this};
    }


    bool operator==(const list& rhs) const {
        if(num_elements != rhs.num_elements) {
            return false;
        } else {
            const_iterator iterThis= begin();
            const_iterator iterRhs = rhs.begin();
            while(iterThis != end()){

                if(iterRhs.current_ptr->val != iterThis.current_ptr->val){
                    return false;
                }
                iterThis++;
                iterRhs++;
            }
            return true;
        }
    }
    bool operator<(const list& rhs) const {
        const_iterator thisIter = this->begin();
        const_iterator rhsIter = rhs.begin();

        if (thisIter.current_ptr == nullptr && rhsIter.current_ptr == nullptr){
            return false;
        } else if (thisIter.current_ptr == nullptr){
            return true;
        } else if (rhsIter.current_ptr == nullptr){
            return false;
        } else if(thisIter.o_list->num_elements < rhsIter.o_list->num_elements){
            return true;
        } else if (thisIter.o_list->num_elements > rhsIter.o_list->num_elements){
            if(thisIter.current_ptr->val < rhsIter.current_ptr->val){
                return true;
            }
            return false;
        } else {
            while(thisIter != this->end()){
                if(thisIter.current_ptr->val < rhsIter.current_ptr->val){
                    return true;
                } else if(thisIter.current_ptr->val > rhsIter.current_ptr->val){
                    return false;
                }
                thisIter++;
                rhsIter++;
            }
            //both lists are equal
            return false;
        }
    }
    bool operator<=(const list& rhs) const {
        if(rhs.operator==(rhs) || rhs.operator<(rhs)){
            return true;
        } else {
            return false;
        }
    }
    bool operator>(const list& rhs) const {
        return !rhs.operator<(rhs);
    }
    bool operator>=(const list& rhs) const {
        return (rhs.operator>(rhs)|| rhs.operator==(rhs));
    }

    std::pair<list, list> split(const_iterator place) {
        list left, right;
        if(place != begin() && place!=end()){
            node* ptr = this->head;
            left.head = ptr;
            while(ptr != place.current_ptr){
                left.num_elements++;
                ptr = ptr->next;
            }
            left.tail = ptr->prev;
            left.tail->next = nullptr;

            ptr->prev = nullptr;

            right.head = ptr;
            right.tail = tail;

            right.num_elements = this->num_elements - left.num_elements;

            //clearing
            this->head = nullptr;
            this->tail = nullptr;
            this->num_elements = 0;

        } else if (place == begin()){
            right.swap(*this);
        } else {

            left.swap(*this);

        }
        return {std::move(left),std::move(right)};
    }



    void my_merge(list& rhs) {
        auto pred = [](const auto& lhs, const auto& rhs) {
            return !(lhs < rhs || lhs == rhs);
        };
        merge(rhs, pred);
    }

    template <typename Comparator>
    void my_merge(list& rhs, Comparator cmp) {
        //1. rhs is empty
        if (rhs.empty()){
            return;
        }

        //2. lhs is empty
        if (empty()){
            join(rhs);
            return;
        }

        //2.a both one


        //3. both non empty
        list::node* lp = head;
        list::node* rp = rhs.head;

        while (!(rp == nullptr && lp->next == nullptr)) {

            //3.1. reached the end of rhs
            if (rp == nullptr){
                return;
            }

            //3.2 l is null pointer
            if(lp == nullptr){
                join(rhs);
                return;
            }

            //3.3 r value is equal or greater than
            if(cmp(rp->val, lp->val)){
                lp = lp->next;
            } else {
                //#1
                list::node* x = rp->next;

                //priradit ten prvek rhs -> lhs
                if (lp->prev != nullptr){
                    //lp prev s rp
                    lp->prev->next = rp;
                    rp->prev = lp->prev;

                    //rp next s lp
                    lp->prev = rp;
                    rp->next = lp;
                }else{
                    head = rp;
                    rp->prev = nullptr;

                    rp->next = lp;
                    lp->prev = rp;
                }
                rp = x;
                rhs.head = rp;
                num_elements++;
                rhs.num_elements--;
            }
        }
    }


    void sort() {
//        auto pred = [](const auto& lhs, const auto& rhs) {
//            return lhs < rhs;
//        };
//        sort(pred);
        //end recursion
        if(num_elements <= 1){
            return;
        }
        //get begin the iterator of the base list
        list::iterator ptr = begin();
        //move the pointer to the middle of the base list
        for(int i = 0; i < static_cast<int>(num_elements/2); i++){
            ptr++;
        }
        //splitting in the middle
        std::pair<list, list> splited = split(ptr);
        //call sorting on both parts until only single items present
        splited.first.sort();
        splited.second.sort();
        //the merging attempt on both sorted lists
        splited.first.my_merge(splited.second);
//        std::merge(splited.first.begin(), splited.first.end(), splited.second.begin(), splited.second.end(),splited.first);
        //moving the sorted list in the base list
        swap(splited.first);
    }

    template <typename Comparator>
    void sort(Comparator cmp){
        //end recursion
//        sort();
        if(num_elements <= 1){
            return;
        }
        //get begin the iterator of the base list
        list::iterator ptr = begin();
        //move the pointer to the middle of the base list
        for(int i = 0; i < static_cast<int>(num_elements/2); i++){
            ptr++;
        }
        //splitting in the middle
        std::pair<list, list> splited = split(ptr);
        //call sorting on both parts until only single items present
        splited.first.sort(cmp);
        splited.second.sort(cmp);
        //the merging attempt on both sorted lists
        splited.first.my_merge(splited.second, cmp);
//        std::merge(splited.first.begin(), splited.first.end(), splited.second.begin(), splited.second.end(),splited.first,cmp);

        //moving the sorted list in the base list
        swap(splited.first);
    }


private:
    node* head;
    node* tail;
    size_t num_elements;
};

/*
* Two lists are not equal iff any of their elements differ.
*/
template <typename T>
bool operator!=(const list<T>& lhs, const list<T>& rhs) {
    return !lhs.operator==(rhs);
}

/*
* ADL customization point for std::swap.
* Should do the same as calling lhs.swap(rhs)
*/
template <typename T>
void swap(list<T>& lhs, list<T>& rhs) {
    lhs.swap(rhs);
}



#endif
