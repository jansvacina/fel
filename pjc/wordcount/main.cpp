//
// Created by jan on 11-Dec-16.
//
#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <map>
#include <chrono>
#include <thread>
#include "CtrlImpl.hpp"
#include "sortImpl.hpp"

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

/*
 * Writing results to file
 */
template <typename T, typename V>
void writeToFile(T* t, V v){
    std::ofstream wordCount;
    wordCount.open(v);
    for (auto it = (*t).begin(); it != (*t).end(); ++it) {
        wordCount << it->first << ": " << it->second << std::endl;
    }
    wordCount.close();
}


/*
 * Convert map to vector
 */
void mapToVec(std::map<std::string, int>* baseMap,std::vector<std::pair<std::string, int>>* baseVec){
    std::copy((*baseMap).begin(),(*baseMap).end(),std::back_inserter((*baseVec)));
}


int main(int argc, char **argv) {

    auto start = std::chrono::high_resolution_clock::now();

    if (argc <= 4) {
        std::cerr << "Not enough arguments provided to be program executable." << std::endl;
        return 0;
    }

    std::string type = argv[1];
    std::string sortVal = argv[2];
    std::string resultPath = argv[3];

    bool parallel = false;

    if(type == "thread"){
        parallel = true;
    }  else {
        if (type != "iter"){
            std::cerr << "Wrong type of key word for parallelism provided." << std::endl;
            return 0;
        }
    }

    if(!(sortVal == "descString" || sortVal == "ascString" || sortVal == "ascInt" || sortVal == "descInt")){
        std::cerr << "Provided a wrong format of sorting key word." << std::endl;
        return 0;
    }

    std::vector<std::unique_ptr<CtrlImpl>> controllers;
    for(int i = 4; i < argc; i++){
        std::unique_ptr<CtrlImpl> ctrl(new CtrlImpl(argv[i]));
        controllers.push_back(std::move(ctrl));
    }

    std::map<std::string, int> baseMap;


    if (parallel) {
        std::cout << "Using threads." << std::endl;
        std::vector<std::thread> ts;
        for(auto& ctrl : controllers){
            ts.push_back(std::thread([&ctrl]() {
                ctrl->parseFile();
            }));
        }
        for(int i = 0; i < ts.size(); ++i){
            if(ts.at(i).joinable()){
                ts.at(i).join();
                controllers.at(i).get()->mergeMaps(&baseMap);
            }
        }
    } else {
        for (int i=0; i<controllers.size(); i++){
            controllers[i].operator*().parseFile();
            controllers[i].operator*().mergeMaps(&baseMap);
        }
    }

    if(sortVal != "ascString"){
        std::cout << "Using sequence." << std::endl;
        std::vector<strIntPair> baseVec;
        mapToVec(&baseMap,&baseVec);
        sortBaseVec(&baseVec, sortVal);
        writeToFile(&baseVec, resultPath);
    } else {
        std::cout << "Sorting ASC by String." << std::endl;
        writeToFile(&baseMap, resultPath);
    }

    auto finish = std::chrono::high_resolution_clock::now();
    std::cout << "Needed " << to_ms(finish - start).count() << " ms to finish.\n";
    return 0;
}