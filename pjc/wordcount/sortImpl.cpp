#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include "sortImpl.hpp"
/*
 * Sorting predicates
 */
typedef std::pair<std::string,int> strIntPair;

bool sortStrDesc(const strIntPair &left, const strIntPair &right){
    return left.first > right.first;
}

bool sortIntAsc(const strIntPair &left, const strIntPair &right){
    return left.second < right.second;
}

bool sortIntDesc(const strIntPair &left, const strIntPair &right){
    return left.second > right.second;
}
/*
 * Sorting function
 */
void sortBaseVec(std::vector<std::pair<std::string, int>>* baseVec, std::string sortVal){
    if(sortVal == "descString"){
        std::cout << "Sorting DESC by String." << std::endl;
        std::sort((*baseVec).begin(), (*baseVec).end(), sortStrDesc);

    } else if (sortVal == "ascInt"){
        std::cout << "Sorting ASC by Int." << std::endl;
        std::sort((*baseVec).begin(), (*baseVec).end(), sortIntAsc);

    } else{
        std::cout << "Sorting DESC by Int." << std::endl;
        std::sort((*baseVec).begin(), (*baseVec).end(), sortIntDesc);
    }
}