//
// Created by jan on 11-Dec-16.
//
#ifndef WORDCOUNT_CTRLIMPL_HPP
#define WORDCOUNT_CTRLIMPL_HPP

#include <string>
#include <map>

class CtrlImpl {
private:

    std::string filePath;

    std::pair<std::map<std::string, int>::iterator, bool> flag;

    std::map<std::string, int>::iterator it = map.begin();

    std::map<std::string, int> map;

    void trimAndClean(std::string* text);

public:

    CtrlImpl(std::string filePath);

    ~CtrlImpl();

    void mergeMaps(std::map<std::string, int> *map);

    void parseFile();
};

#endif
