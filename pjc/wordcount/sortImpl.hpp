#ifndef WORDCOUNT_SORTIMPL_HPP
#define WORDCOUNT_SORTIMPL_HPP
void sortBaseVec(std::vector<std::pair<std::string, int>>* baseVec, std::string sortVal);

typedef std::pair<std::string,int> strIntPair;

bool sortStrDesc(const strIntPair &left, const strIntPair &right);

bool sortIntAsc(const strIntPair &left, const strIntPair &right);

bool sortIntDesc(const strIntPair &left, const strIntPair &right);

#endif
