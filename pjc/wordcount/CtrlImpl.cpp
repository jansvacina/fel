//
// Created by jan on 11-Dec-16.
//
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include "CtrlImpl.hpp"

CtrlImpl::CtrlImpl(std::string fileName) : filePath(fileName) {}

CtrlImpl::~CtrlImpl() {}

void CtrlImpl::mergeMaps(std::map<std::string, int>* baseMap) {
    for (this->it = this->map.begin(); it != this->map.end(); ++it) {
        flag = (*baseMap).insert(std::pair<std::string, int>(it->first, it->second));
        if (flag.second == false) {
            flag.first->second += it->second;
        }
    }
}

void CtrlImpl::parseFile() {
    std::ifstream ifStream(filePath);
    if (ifStream.is_open()) {
        std::string word;
        while (ifStream >> word) {
            trimAndClean(&word);
            map[word]++;
        }
        ifStream.close();
    } else {
        std::cerr << "File is not reachable or readable!" << std::endl;
    }
}

void CtrlImpl::trimAndClean(std::string* text) {
    (*text).erase (std::remove_if ((*text).begin (), (*text).end (), ispunct), (*text).end ());
    std::transform ((*text).begin(), (*text).end(), (*text).begin(), ::tolower);
}





