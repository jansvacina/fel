/**
 * Created by jan on 08-Mar-17.
 */
public class Homework1 implements Mergesort {

    @Override
    public int[] getFirstHalfOf(int[] array) {
        int arrayLength = array.length / 2;
        if(!(array.length % 2 == 0)){
            arrayLength++;
        }
        int[] newArray = new int[arrayLength];
        System.arraycopy(array, 0, newArray, 0, arrayLength);
        return newArray;
    }

    @Override
    public int[] getSecondHalfOf(int[] array) {
        int[] newArray = new int[array.length / 2];
        System.arraycopy(array, array.length - array.length / 2, newArray, 0, array.length / 2);
        return newArray;
    }

    @Override
    public int[] merge(int[] firstHalf, int[] secondHalf) {
        int[] tmp = new int[firstHalf.length + secondHalf.length];

        int i = 0 ,j = 0 ,k = 0;

        while(k != tmp.length){
            if(i < firstHalf.length && j < secondHalf.length){
                if(firstHalf[i] < secondHalf[j]){
                    tmp[k] = firstHalf[i];
                    i++;
                } else {
                    tmp[k] = secondHalf[j];
                    j++;
                }
            } else {
                if(j < secondHalf.length){
                    tmp[k] = secondHalf[j];
                    j++;
                } else {
                    tmp[k] = firstHalf[i];
                    i++;
                }
            }
            k++;
        }
        return tmp;
    }

    @Override
    public int[] mergesort(int[] array) {

        if(array.length <= 1){
            return array;
        }
        int[] firstArray = getFirstHalfOf(array);
        int[] secondArray = getSecondHalfOf(array);

        firstArray = mergesort(firstArray);
        secondArray = mergesort(secondArray);

        return merge(firstArray,secondArray);
    }
}
