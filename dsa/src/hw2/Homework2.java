
/**
 * Created by jan on 10-Mar-17.
 */

/*
 * Naimplementujte třídu Homework2 implementující rozhraní InterSearch (váš soubor Homework2.java bude toto rozhraní obsahovat).
 */

interface InterSearch {
    /* Požitím interpolačního hledání vrátí index prvku what nalezeného mezi indexy first a last
       pole data nebo -1, pokud tam není. Metoda bude rekurzivní a měla by být odolná vůči chybně
       zadaným parametrům (v případě chyby vrátí opět -1). Pro zaokrouhlování na celá čísla použijte metodu Math.round(). */
    public int search(int first, int last, int what, int[] data);
}

public class Homework2 implements InterSearch {


    /**
     *
     * @param first index, begin of the data array
     * @param last index, end of the data array
     * @param what number, that we are looking for
     * @param data set of numbers in which searching is performed
     * @return index of the item, we were looking for, found in between first and last index of the data array
     */
    @Override
    public int search(int first, int last, int what, int[] data) {

        if(first > last || first < 0 || last < 0 || data.length < 0 || data.length  <= last || what > data[last] || what < data[first]){
            return -1;
        }

        if(first == last || data[first] == data[last]){
            if(data[first] == what) {
                return first;
            }
            return -1;
        }

        if(data[last] - data[first] == 0){
            return -1;
        }

        int mid = first + (int) Math.round(((double) (last - first) / (data[last] - data[first])) * (what - data[first]));


        if(data[mid] == what){
            return mid;
        } else if (data[mid] < what){
            first = mid + 1;
            return search(first, last, what, data);
        } else {
            last = mid - 1;
            return search(first, last, what, data);
        }

    }

    public int defineIndex(int first, int last, int what, int[] data){
        int index = (first + (int) (Math.round( (double)((last - first)/(data[last] - data[first])) * (what - data[first]))));
        return index;
    }
}