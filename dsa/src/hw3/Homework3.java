/**
 * Created by jan on 11-Apr-17.
 */

class Homework3 <E extends DSAComparable<E>> implements HeapStorage<E>{

    private E[] elements;
    private int lastIndex;

    Homework3(E[] elements) {
        this.elements = elements;
        this.lastIndex = elements.length - 1;
    }

    @Override
    public int getSize() {
        return lastIndex +1;
    }

    @Override
    public boolean isEmpty() {
        return lastIndex ==-1;
    }

    @Override
    public E getElement(int index) {
        if(index == 0){
            return elements[0].getGreatestElement();
        } else if(index>getSize()){
            return elements[0].getLeastElement();
        } else {
            return elements[index-1];
        }

    }

    @Override
    public void swap(int index, int index2) {
        E tmp = elements[index-1];
        elements[index-1] = elements[index2-1];
        elements[index2-1] = tmp;
    }

    @Override
    public E extractLast() {
        E tmp = elements[lastIndex];
        elements[lastIndex] = null;
        lastIndex--;
        return tmp;
    }

    @Override
    public int insertLast(E element) {
        lastIndex++;
        elements[lastIndex] = element;
        return lastIndex +1;
    }

}

class Heap<E extends DSAComparable<E>>{
    HeapStorage<E> storage;

    Heap(HeapStorage<E> storage){

        this.storage = storage;

        for(int i = storage.getSize(); i >= 1; i--){
            heapify(i);
        }
    }

    /** Zavola algoritmus heapify nad uzlem na indexu index. */
    void heapify(int index) {
        int l = left(index);
        int r = right(index);
        int largest = index;

        if(storage.getElement(index).less(storage.getElement(l))){
            largest = l;
        }

        if(storage.getElement(largest).less(storage.getElement(r))){
            largest = r;
        }

        if(largest != index){
            storage.swap(index, largest);
            heapify(largest);
        }

    }

    /** Vlozi do haldy novy prvek. Muzete predpokladat, ze v poli uvnitr HeapStorage na nej misto je. */
    void insert(E element) {
        int index = storage.insertLast(element);
        increaseKey(index);
    }

    private void increaseKey(int index){
        while (index > 1 && storage.getElement(parent(index)).less(storage.getElement(index))){
            storage.swap(parent(index),index);
            index = parent(index);
        }
    }

    private int parent(int i){
        return i/2;
    }

    int left(int i){ return 2*i; }

    int right(int i){ return 2*i + 1; }

    /** Odstrani a vrati z haldy maximalni prvek. */
    E extractMax() {

        if(storage.getSize() < 1){
            System.err.println("Heap underflow");
        }

        storage.swap(1, storage.getSize());
        E max = storage.extractLast();
        if(!isEmpty()) {
            heapify(1);
        }
        return max;
    }

    /** Vrati true, pokud je halda prazdna. */
    boolean isEmpty() {
        return storage.isEmpty();
    }

    /** Pomoci algoritmu trideni haldou vzestupne setridi pole array. */
    static <E extends DSAComparable<E>> void heapsort(E[] array) {
        Heap heap = new Heap(new Homework3(array.clone()));

        for(int i=array.length-1; i>=0; i--){
            array[i] = (E) heap.extractMax();
        }
    }
}