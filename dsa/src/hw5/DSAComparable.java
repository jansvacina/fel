/*
 * Naimplementujte třídu Homework5 implementující Tree. Odevzdejte pouze kód této třídy.
 */

import java.util.ArrayList;
import java.util.Iterator;


// interface DSAComparable<E extends DSAComparable<E>> {

//     boolean less (E other);

//     boolean greater (E other);

//     boolean equal (E other);

// }


// class Node<E extends DSAComparable<E>> {

//     E contents;

//     private Node<E> parent;

//     Node<E> left, right;

//     // Vrati naslednika (tzn. uzel obsahujici nejblizssi vyssi hodnotu) tohoto uzlu.

//     Node<E> succ() {

//         if (right != null ) {

//             Node<E> temp = right;

//             while (temp.left != null ) temp = temp.left;

//             return temp;

//         } else {

//             Node<E> temp = this;

//             Node<E> parent = this.parent;

//             while (parent != null && parent.right == temp) {

//                 parent = parent.parent;

//                 temp = temp.parent;

//             }

//             return parent;

//         }

//     }

//     Node(E elem, Node<E> parent) {

//         this.contents = elem;

//         this.parent = parent;

//     }

//     void setParent(Node<E> p) {

//         parent = p;

//     }

// }


// Trida Tree reprezentuje binarni vyhledavaci strom, ve kterem pro kazdy uzel n plati
// n.left == null || n.left.contents.less(n.contents) a
// n.right == null || n.right.contents.greater(n.contents).

class Tree<E extends DSAComparable<E>> {

    Node<E> root;

    Node<E> tmp = this.root;

    ArrayList<E> list = new ArrayList<E>();

    // Vrati minimum z tohoto stromu nebo null, pokud je strom prazdny.

    E minimum() {

        /* kod */
        return subtreeMin(this.root);
    }

    // Vrati minimum ze zadaneho podstromu nebo null, pokud je podstrom prazdny.

    E subtreeMin(Node<E> n) {
        /* kod */

        if(n.contents == null){
            return null;
        }

        while (n.left != null) {
            n = n.left;
        }

        return n.contents;

    }

    // Vrati maximum z tohoto podstromu nebo null, pokud je podstrom prazdny.

    E maximum() {

        /* kod */

        return subtreeMax(this.root);

    }

    // Vrati maximum ze zadaneho podstromu nebo null, pokud je podstrom prazdny.

    E subtreeMax(Node<E> n) {

        /* kod */
        if(n.contents == null){
            return null;
        }

        while (n.right != null) {
            n = n.right;
        }

        return n.contents;

    }

    // Vlozi prvek do stromu (duplicity jsou zakazane)

    void insert(E elem) {
        /* kod */
        Node<E> tmp = null;
        Node<E> root = this.root;
        Node<E> newNode = new Node<E>(elem, null);
        while (root != null) {

            tmp = root;

            if (newNode.contents.less(root.contents)){
                root = root.left;
            } else if(newNode.contents.equal(root.contents)) {
                return;
            }
            else {
                root = root.right;
            }
        }
        newNode.setParent(tmp);

        if(tmp == null) {
            this.root = newNode;
        } else if (newNode.contents.less(tmp.contents)){
            tmp.left = newNode;
        } else if (newNode.contents.greater(tmp.contents)){
            tmp.right = newNode;
        } else {
            newNode = new Node<E>(null, null);
        }

    }

    // Projde strom a vrati:
    // - uzel s hodnotou elem, pokud existuje,
    // - null pokud uzel s hodnotou elem neexistuje

    Node<E> find(E elem) {
        /* kod */

        Node<E> tmp = this.root;

        if (elem == null){
            return null;
        }

        while((tmp != null)&&(!(elem.equal(tmp.contents)))){
            if (elem.less(tmp.contents)){
                tmp = tmp.left;
            } else {
                tmp = tmp.right;
            }
        }

        if(tmp == null){
            return null;
        }

        if(elem.equal(tmp.contents)){
            return tmp;
        } else {
            return null;
        }

    }

    // Vrati true, pokud tento strom obsahuje prvek elem.

    boolean contains(E elem) {

        /* kod */

        Node<E> tmp = this.find(elem);

        return (tmp != null) ? true : false;

    }

    //Vrati parent daneho uzlu

    Node<E> getParent(Node<E> child){
        Node<E> tmp = this.root;
        Node<E> parent = null;
        if(tmp == null){
            return null;
        }
        while(tmp != null && !(child.contents.equal(tmp.contents))){
            if (child.contents.less(tmp.contents)){
                parent = tmp;
                tmp = tmp.left;
            } else{
                parent = tmp;
                tmp = tmp.right;
            }
        }
        return parent;
    }

    //Nahradi jeden podstrom jako potomek predchoziho

    void transplant(Node<E> u, Node<E> v){
        Node<E> uParent = getParent(u);
        if(uParent == null){
            this.root = v;
        } else if (u == uParent.left){
            uParent.left = v;
        } else {
            uParent.right = v;
        }
        if (v != null){
            Node<E> vParent = getParent(v);
            vParent = uParent;
        }
    }

    // Odstrani vyskyt prvku elem z tohoto stromu.

    void remove(E elem) {
        /* kod */
        Node<E> z = find(elem);
        if(z == null){
            return;
        }
        if(z.left == null){
            transplant(z,z.right);
        } else if (z.right == null){
            transplant(z,z.left);
        } else {
            Node <E> y = subtreeMinNode(z.right);
            if(getParent(y)!= z){
                transplant(y,y.right);
                y.right = z.right;
            }
            transplant(z,y);
            y.left = z.left;
        }

    }


    Node<E> subtreeMinNode(Node<E> n) {
        /* kod */

        if(n.contents == null){
            return null;
        }

        while (n.left != null) {
            n = n.left;
        }

        return n;

    }


    // Vrati iterator pres cely strom (od nejmensiho po nejvetsi). Metoda remove() nemusí být implementována

    Iterator<E> iterator() {

        /* kod */

        Node<E> root = this.root;
        this.list = new ArrayList<E>(){};
        Iterator<E> iterator = new Iterator<E>() {
            private int pointer = 0;
            private ArrayList<E> array = inOrder(root);

            @Override
            public boolean hasNext() {
                if (array.size() == 0) {
                    return false;
                } else if (pointer >= array.size()) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public E next() {
                pointer++;
                return array.get(pointer - 1);
            }
        };

        return iterator;

    }

    public ArrayList<E> inOrder(Node<E> tmp){

        if (tmp != null) {
            inOrder(tmp.left);
            this.list.add(tmp.contents);
            inOrder(tmp.right);
        }

        return this.list;
    }

    // Vrati korenovy uzel tohoto stromu.

    Node<E> getRootNode() {

        /* kod */

        return this.root;
    }

}