/**
 * Created by jan on 24-Apr-17.
 */
/*
 *  V souboru Homework4.java naimplementujte třídu DSAHashTable.
 *  Kvůli omezení Javy musíte generické pole alokovat takto:
 *  (Set<Pair<K, V> >[]) new Set<?>[size]. Odevzdejte pouze kód této třídy.
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import java.util.Set;


// Trida Pair reprezentuje dvojici (klic, hodnota).

//class Pair<K,V> {
//
//    K key;
//
//    V value;
//
//    Pair(K k, V v) {
//
//        key = k;
//
//        value = v;
//
//    }
//
//    public int hashCode() {
//
//        return key.hashCode();
//
//    }
//
//    public boolean equals(Object other) {
//
//        if (! (other instanceof Pair<?,?>)) return false;
//
//        Pair<?,?> o = (Pair<?,?>) other;
//
//        return key.equals(o.key) && value.equals(o.value);
//
//    }
//
//}


// Trida DSAHashTable reprezentuje rozptylovaci tabulku se zřetězením (první varianta v učebnici).

class DSAHashTable<K,V> {

    int size;

    HashSet<Pair<K,V>> keyValueSet[];

    HashSet<Pair<K,V>> oldKeyValueSet[];

    int elementCount;

    Iterator iterator;

    Pair<K,V> suspectedPair;

    // Vytvori prazdnou instanci DSAHashTable, delka vnitrniho pole je nastavena na size, obsah vnitrniho pole je inicializovan na prazdne mnoziny.

    DSAHashTable(int size) {

        /* kod */

        this.size = size;

        keyValueSet = (HashSet<Pair<K,V>>[])new HashSet<?>[size];

        for(int i = 0; i < this.size; i ++){

            keyValueSet[i] = new HashSet<Pair<K,V>>();

        }

        this.elementCount = 0;
    }

    // Ulozi dvojici (key, value) do rozptylovaci tabulky. Pokud uz v tabulce je jina dvojice se stejnym klicem, je smazana.
    // Klic ani hodnota nesmi byt null. Pokud by pocet dvojic v tabulce po vykonani put mel vzrust nad dvojnasobek delky vnitrniho pole,
    // vnitrni pole zdvojnasobi.

    void put(K key, V value) {

        /* kod */

        if ( key == null || value == null ) {

            return;

        }


        if (  this.get(key) != null ) {

            this.remove(key);

        }

        Pair<K,V> newPair = new Pair<K, V>(key,value);


        this.keyValueSet[this.getIndexOf(key)].add(newPair);

        this.elementCount++;

        if (! isBigEnough()) {

            this.resize(2 * this.size );

        }

    }

    // Vrati hodnotu asociovanou s danym klicem nebo null, pokud dany klic v tabulce neni.

    V get(K key) {

        /* kod */

        if( key == null) {

            return null;

        } else {

            this.iterator = this.keyValueSet[this.getIndexOf(key)].iterator();

            while(iterator.hasNext()){

                this.suspectedPair = (Pair<K, V>) this.iterator.next();

                if( this.suspectedPair.key.equals(key)){

                    return (V) this.suspectedPair.value;

                }

            }

        }

        return null;
    }


    // Smaze dvojici s danym klicem. Pokud v tabulce dany klic neni, nedela nic.

    void remove(K key) {

        /* kod */

        if( key == null ) {

            return;

        }

        int index = getIndexOf(key);

        this.iterator = this.keyValueSet[index].iterator();

        while (iterator.hasNext()){

            this.suspectedPair = (Pair<K,V>) iterator.next();

            if ( this.suspectedPair.key.equals(key) ){

                keyValueSet[index].remove(suspectedPair);

                this.elementCount--;

                break;

            }
        }
    }

    // Vrati vnitrni pole. Prvky vnitrniho pole mohou byt instance trid v balicku java.util, tzn. nemusite psat vlastni implementaci rozhrani java.util.Set.

    Set<Pair<K,V>>[] getArray() {

        /* kod */

        return keyValueSet;

    }

    // Pro dany klic vrati index v poli. Jako hashovaci funkce se pouzije key.hashCode.

    int getIndexOf(K key) {

        /* kod */

        return Math.abs(key.hashCode())%this.size;
    }

    // Pokud je pocet prvku mensi nebo roven dvojnasobku delky vnitrniho pole, vrati true, jinak vrati false.

    boolean isBigEnough() {

        /* kod */

        return (this.elementCount <= ( 2 * this.size)) ? true : false;

    }

    // Zmeni delku vnitrniho pole, nainicializuje jej prazdnymi mnozinami a zkopiruje do nej vsechny dvojice.

    void resize(int newSize) {

       /* kod */

        this.oldKeyValueSet = this.keyValueSet;

        this.keyValueSet = (HashSet<Pair<K, V> >[]) new HashSet<?>[newSize];

        this.size = newSize;

        this.elementCount = 0;

        for ( int i = 0; i < this.keyValueSet.length; i++ ){

            keyValueSet[i] = new HashSet<Pair<K, V>>();
        }
        

        for (HashSet<Pair<K, V>> hash: this.oldKeyValueSet
                ) {

            Iterator iterator = hash.iterator();

            while (iterator.hasNext()){

                this.suspectedPair = (Pair<K, V>) iterator.next();

                this.put( this.suspectedPair.key, this.suspectedPair.value);


            }
        }

    }

    // Vrati iterator pres vsechny dvojice v tabulce. Iterator nemusi mit implementovanou metodu remove.

    Iterator<Pair<K,V>> iterator() {

        /* kod */

        ArrayList<Pair<K,V>> arrayList = new ArrayList<Pair<K, V>>();

        for (HashSet<Pair<K,V>> hashSet: this.keyValueSet
             ) {
            this.iterator = hashSet.iterator();

            while (this.iterator.hasNext()) {

                this.suspectedPair = (Pair<K,V>) this.iterator.next();

                arrayList.add(this.suspectedPair);

            }
        }

        return arrayList.iterator();
    }
}
