import java.util.HashSet;
import java.util.Set;

/**
 * Created by jan on 27-Oct-16.
 */
public class Main {
    public static void main(String[] args) {
        OMOSet a = new OMOSet();
        OMOSet b = new OMOSet();
        a.add(0);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);

       b.add(0);
        b.add(3);
        b.add(4);
        b.add(5);
        b.add(7);
        b.add(8);b.add(9);
        b.add(10);
        b.add(5);

        int containVal = 4;

        int [] aarr = a.toArray();
        System.out.print("Array A: ");
        print(aarr);

        System.out.println("OMOSet contain test");
        OMOSet oset = new OMOSet();
        Set<Integer> hs = new HashSet<Integer>();
        hs.add(0);
        hs.add(2);
        hs.add(3);
        hs.add(4);
        hs.add(5);
        oset.setSet(hs);
        System.out.println("Array A contains " + containVal + "> " + oset.contains(containVal));


        int [] barr = b.toArray();
        System.out.print("Array B: ");
        print(barr);

        OMOSetComplement complement = new OMOSetComplement(a,b);
        System.out.print("Complement: ");
        print(complement.toArray());
        System.out.println("Complement contain " + containVal +" : " + complement.contains(containVal));

        OMOSetIntersection intersection = new OMOSetIntersection(a,b);
        System.out.print("Intersection: ");
        print(intersection.toArray());
        System.out.println("Intersection contain " + containVal +" : " + intersection.contains(containVal));


        OMOSetEven even = new OMOSetEven(a);
        System.out.print("Even: ");
        print(even.toArray());
        System.out.println("Even contain " + containVal +" : " + even.contains(containVal));


        OMOSetUnion union = new OMOSetUnion(a,b);
        System.out.print("Union: ");
        print(union.toArray());
        System.out.println("Union contain " + containVal +" : " + union.contains(containVal));


    }
    public static void print (int [] aarr){
        for(int i = 0; i < aarr.length; i++){
            System.out.print(aarr[i] + ",");
        }
        System.out.println();
    }
}


