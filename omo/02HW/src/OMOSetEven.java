import java.util.HashSet;
import java.util.Set;

// třída reprezentující množinu sudých čísel
class OMOSetEven implements OMOSetView {

    private OMOSetView setA;

    OMOSetEven(OMOSetView setA) {
        this.setA = setA;
    }

    OMOSetEven(){

    }

    public OMOSetView getSetA() {
        return setA;
    }

    public void setSetA(OMOSetView setA) {
        this.setA = setA;
    }

    // metody rozhraní OMOSetView
    @Override
    public boolean contains(int element) {
        if(element % 2 == 0){
            return  setA.contains(element);
        }
        return false;
//        int [] c = this.toArray();
//        for (int v: c
//                ) {
//            if (v == element){
//                return true;
//            }
//        }

    }

    @Override
    public int[] toArray() {

        int [] ar = setA.toArray();
        Set<Integer> inter = new HashSet<Integer>();

        int counter = 0;
        for (int i = 0; i < ar.length; i++){
            if (ar[i] % 2 == 0){
                inter.add(ar[i]);
            }
        }
        int counter2 = 0;
        int [] intarr = new int[inter.size()];
        for (Integer value: inter
                ) {
            intarr[counter2] = value;
            counter2++;
        }
        return intarr;
    }

    @Override
    public OMOSetView copy() {
        OMOSetEven omo = new OMOSetEven();

        OMOSetView newA = this.getSetA().copy();

        omo.setSetA(newA);

        return omo;
    }

}