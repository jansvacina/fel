import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
// třída reprezentující průnik dvou množin: A průnik B

class OMOSetIntersection implements OMOSetView {

   private OMOSetView setA, setB;

   OMOSetIntersection(OMOSetView setA, OMOSetView setB) {
      this.setA = setA;
      this.setB = setB;
   }

   OMOSetIntersection(){

   }

   public OMOSetView getSetA() {
      return setA;
   }

   public void setSetA(OMOSetView setA) {
      this.setA = setA;
   }

   public OMOSetView getSetB() {
      return setB;
   }

   public void setSetB(OMOSetView setB) {
      this.setB = setB;
   }

   @Override
   public boolean contains(int element) {

      int [] c = this.toArray();
      for (int v: c
              ) {
         if (v == element){
            return true;
         }

      }
      return false;
   }
   @Override
   public int[] toArray() {
      int [] ar = setA.toArray();
      int [] br = setB.toArray();
      Set<Integer> inter = new HashSet<Integer>();

      int counter = 0;
      for (int i = 0; i < ar.length; i++){
         if (setB.contains(ar[i])){
            inter.add(ar[i]);
         }
      }
      int counter2 = 0;
      int [] intarr = new int[inter.size()];
      for (Integer value: inter
           ) {
         intarr[counter2] = value;
         counter2++;
      }
      return intarr;
   }

   @Override
   public OMOSetView copy()
   {

      OMOSetIntersection omo = new OMOSetIntersection();

      OMOSetView newA = this.getSetA().copy();
      OMOSetView newB = this.getSetB().copy();

      omo.setSetA(newA);
      omo.setSetB(newB);

      return omo;


   }

// metody rozhraní OMOSetView


}