import com.sun.deploy.util.ArrayUtil;

import java.util.HashSet;
import java.util.Set;

// třída reprezentující obecnou množinu, definuje metody add/remove pro přidávání/odebírání prvků
class OMOSet implements OMOSetView {

    private Set<Integer> set = new HashSet<Integer>();

    public void add(int element) {
        set.add(element);
    }

    public void remove(int element) {
        set.remove(element);
    }

    public OMOSet() {
    }
    public OMOSet(HashSet<Integer> hs) {
        this.set = hs;
    }

    public Set<Integer> getSet() {
        return set;
    }

    public void setSet(Set<Integer> set) {
        this.set = set;
    }

    @Override
    public boolean contains(int element) {
        return getSet().contains(element);
    }

    @Override
    public int[] toArray() {
        int[] array = new int[getSet().size()];
        int counter = 0;
        for (Integer value: getSet()
                ){
            array [counter] = value;
            counter++;
        }
        return array;


    }

    @Override
    public OMOSetView copy()

    {
        OMOSet a = new OMOSet();
        Set<Integer> newSet = new HashSet<Integer>();
        for(Integer i : this.getSet())
            newSet.add(i);
        a.setSet(newSet);
        return a;
    }
}

