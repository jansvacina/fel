import java.util.HashSet;
import java.util.Set;

// třída reprezentující sjednocení dvou množin: A sjednoceno B
class OMOSetUnion implements OMOSetView {

    private OMOSetView setA, setB;

    OMOSetUnion(OMOSetView setA, OMOSetView setB) {
        this.setA = setA;
        this.setB = setB;
    }

    OMOSetUnion(){

    }

    public OMOSetView getSetA() {
        return setA;
    }

    public void setSetA(OMOSetView setA) {
        this.setA = setA;
    }

    public OMOSetView getSetB() {
        return setB;
    }

    public void setSetB(OMOSetView setB) {
        this.setB = setB;
    }

    // metody rozhraní OMOSetView
    @Override
    public boolean contains(int element) {


        return setA.contains(element) || setB.contains(element);
    }

    @Override
    public int[] toArray() {
        int [] ar = setA.toArray();
        int [] br = setB.toArray();
        Set<Integer> inter = new HashSet<Integer>();

        for(int k = 0; k < ar.length; k++){
            inter.add(ar[k]);
        }

        int counter = 0;
        for (int i = 0; i < br.length; i++){
            if (!setA.contains(br[i])){
                inter.add(br[i]);
            }
        }
        int counter2 = 0;
        int [] intarr = new int[inter.size()];
        for (Integer value: inter
                ) {
            intarr[counter2] = value;
            counter2++;
        }
        return intarr;
    }

    @Override
    public OMOSetView copy() {
        OMOSetUnion omo = new OMOSetUnion();

        OMOSetView newA = this.getSetA().copy();
        OMOSetView newB = this.getSetB().copy();

        omo.setSetA(newA);
        omo.setSetB(newB);

        return omo;
    }
}