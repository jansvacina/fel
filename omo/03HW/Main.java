/**
 * Created by jan on 09-Nov-16.
 */

interface CustomIterator {
    boolean hasNext();
    int next();
}

class Node {
    final int contents;
    final Node left, right;
    Node parent;

    Node(int contents, Node left, Node right) {
        this.contents = contents;
        this.left = left;
        if (left != null) left.parent = this;
        this.right = right;
        if (right != null) right.parent = this;
    }

    CustomIterator preorderIterator() {
        CustomIteratorImpl cii = new CustomIteratorImpl(parent);
        return new CustomIteratorImpl(parent);
    }
    CustomIterator inorderIterator() {
        CustomIteratorImpl cii = new CustomIteratorImpl(parent);
        return new CustomIteratorImpl(parent);
    }
    CustomIterator postorderIterator() {
        CustomIteratorImpl cii = new CustomIteratorImpl(parent);
        return new CustomIteratorImpl(parent);

    }
}

class CustomIteratorImpl implements CustomIterator {

    private Node nextNode;

    public CustomIteratorImpl(Node nextNode) {
        this.nextNode = nextNode;
    }

    @Override
    public boolean hasNext() {
        if(nextNode.left == null && nextNode.right == null){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int next() {
        return nextNode.contents;
    }
}