import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by jan on 09-Nov-16.
 */

interface CustomIterator {
    boolean hasNext();
    int next();
}

class Node {
    final int contents;
    final Node left, right;
    Node parent;

    Node(int contents, Node left, Node right) {
        this.contents = contents;
        this.left = left;
        if (left != null) left.parent = this;
        this.right = right;
        if (right != null) right.parent = this;
    }

    CustomIterator preorderIterator() {
        return new CustomIteratorPreImpl(this);
    }
    CustomIterator inorderIterator() {
        return new CustomIteratorInImpl(this);
    }
    CustomIterator postorderIterator() {
        return new CustomIteratorPostImpl(this);

    }
}

class CustomIteratorPreImpl implements CustomIterator {

    Deque<Integer> stack = new ArrayDeque<Integer>();

    private Node nextNode;

    public CustomIteratorPreImpl(Node nextNode) {
        this.nextNode = nextNode;
        preorder(nextNode);
        print();
    }

    public void preorder(Node node){
        if (node != null){
            stack.add(node.contents);
            preorder(node.left);
            preorder(node.right);
        }
    }

    public void print(){
        for (Integer i: stack
                ) {
            System.out.print(i + ",");
        }
        System.out.println();
    }

    @Override
    public boolean hasNext() {
        if(nextNode.left == null && nextNode.right == null){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int next() {
//        return ad.pollLast();
        return 0;
    }
}

class CustomIteratorInImpl implements CustomIterator{

    Deque<Integer> stack = new ArrayDeque<Integer>();

    private Node nextNode;

    public CustomIteratorInImpl(Node nextNode) {
        this.nextNode = nextNode;
        inorder(nextNode);
        print();
    }

    public void inorder(Node node){
        if (node != null){
            inorder(node.left);
            stack.add(node.contents);
            inorder(node.right);

        }
    }

    public void print(){
        for (Integer i: stack
                ) {
            System.out.print(i + ",");
        }
        System.out.println();
    }


    @Override
    public boolean hasNext() {
        if(nextNode.left == null && nextNode.right == null){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int next() {
        if(!stack.isEmpty()){
            return stack.pop();
        } else {
            return 0;
        }

    }
}

class CustomIteratorPostImpl implements CustomIterator{

    Deque<Integer> stack = new ArrayDeque<Integer>();

    private Node nextNode;

    public CustomIteratorPostImpl(Node nextNode) {
        this.nextNode = nextNode;
        postorder(nextNode);
        print();
    }

    public void postorder(Node node){
        if (node != null){
            postorder(node.left);
            postorder(node.right);
            stack.add(node.contents);
        }
    }

    public void print(){
        for (Integer i: stack
                ) {
            System.out.print(i + ",");
        }
        System.out.println();

    }

    @Override
    public boolean hasNext() {
        if(!stack.isEmpty()){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int next() {
        if(!stack.isEmpty()){
            return stack.pop();
        } else {
            return 0;
        }

    }
}


public class Main{
    public static void main(String[] args) {
        Node root = new Node(2, new Node(1, new Node(4, null, null), new Node(5, null, null)), new Node(3, new Node(6, null, null), new Node(7, null, null)));

        System.out.println("pre order ");
        root.preorderIterator();
        System.out.println();
        System.out.println("post order ");
        root.postorderIterator();
        System.out.println();
        System.out.println("in order ");
        root.inorderIterator();
        System.out.println();
        System.out.println("pre order via next");
        System.out.print(root.postorderIterator().next());
        System.out.print(root.postorderIterator().next());


        System.out.println("testing pop");

        Deque<Integer> mystack = new ArrayDeque<Integer>();
        mystack.add(1);
        mystack.add(2);
        mystack.add(3);
        System.out.println(mystack.pop());
        System.out.println(mystack.pop());
        System.out.println(mystack.pop());

        CustomIterator postorderIterator =
             new CustomIteratorPostImpl(root);

        System.out.println(postorderIterator.next());
        System.out.println(postorderIterator.next());
        System.out.println(postorderIterator.next());System.out.println(postorderIterator.next());
        System.out.println(postorderIterator.hasNext());
        System.out.println(postorderIterator.next());System.out.println(postorderIterator.next());
        System.out.println(postorderIterator.next());System.out.println(postorderIterator.next());
        System.out.println(postorderIterator.hasNext());


    }
}

