import jdk.nashorn.internal.ir.annotations.Immutable;

import java.util.function.Predicate;
import.java.util.iterator;

/**
 * Created by jan on 02-Jan-17.
 */
//interface Movable {
//    /** Vrati aktualni polohu. */
//    Pair<Integer, Integer> getPosition();
//
//    /** Pohne timto objektem o dx pixelu doprava a dy dolu. */
//    void move(int dx, int dy);
//}
//
//interface Actuator {
//    /** Pohne objektem v parametru. */
//    void actuate(Movable movable);
//}
//
///** Instance tridy Pair reprezentuji dvojice hodnot. */
//class Pair<A, B> {
//    final A first;
//    final B second;
//
//    public Pair(A first, B second) {
//        this.first = first;
//        this.second = second;
//    }
//}

class Up implements Actuator {

    @Override
    public void actuate(Movable movable) {
        movable.move(0,-1);
    }
}

class Down implements Actuator {

    @Override
    public void actuate(Movable movable) {
        movable.move(0,1);
    }
}

class Left implements Actuator {

    @Override
    public void actuate(Movable movable) {
        movable.move(-1,0);
    }
}

class Right implements Actuator {

    @Override
    public void actuate(Movable movable) {
        movable.move(1,0);
    }
}

class ParallellyComposingActuator implements Actuator {

    private ImmutableList<Actuator> motors;

    public ParallellyComposingActuator(ImmutableList<Actuator> motors) {
            this.motors = motors;
    }

    @Override
    public void actuate(Movable movable) {
        for (Actuator a: motors
             ) {
            a.actuate(movable);
        }
    }
}

class AmplifyingActuator implements Actuator {

    private Actuator motor;

    private int factor;

    public AmplifyingActuator(Actuator motor, int factor) {
        this.motor = motor;
        this.factor = factor;
    }

    @Override
    public void actuate(Movable movable) {
        for(int i = 0; i < factor; i++){
            motor.actuate(movable);
        }

    }
}

class SequentiallyComposingActuator implements Actuator {

    private Iterable<Pair<Predicate<Movable>, Actuator>> iterable;

    private Pair<Predicate<Movable>, Actuator> pair;


    public SequentiallyComposingActuator(Iterable<Pair<Predicate<Movable>, Actuator>> iterable) {
        this.iterable = iterable;
        this.pair = this.iterable.next();
    }

    @Override
    public void actuate(Movable movable) {
        if(pair.first.apply(movable)){
            pair.second.actuate(movable);
        } else {
            pair = iterable.iterator().next();
            actuate(movable);
        }
    }
}

//class Keyboard {
//    static boolean leftArrowPressedDown() {
//        /* kod */
//    }
//
//    static boolean rightArrowPressedDown() {
//        /* kod */
//    }
//}

class Homework5 {


    static Actuator getFirstActuator() {
        return new Actuator() {
            @Override
            public void actuate(Movable movable) {

                new Right().actuate(movable);
                new Up().actuate(movable);

            }
        };

    }

    static Actuator getSecondActuator() {
        return new Actuator() {
            @Override
            public void actuate(Movable movable) {
                if(Keyboard.leftArrowPressedDown()){
                    new Left().actuate(movable);
                } else if (Keyboard.rightArrowPressedDown()){
                    new Right().actuate(movable);
                } else {
                    movable.move(0,0);
                }
            }
        };
    }


    static Actuator getThirdActuator(final Actuator firstActuator, Actuator secondActuator) {

        return new InnerActuator(firstActuator,secondActuator);
    }

    private static class InnerActuator implements Actuator {

        Actuator firstActuator = null;
        Actuator secondActuator = null;

        int counter = 1;

        public InnerActuator(final Actuator firstActuator, Actuator secondActuator) {
            this.firstActuator = firstActuator;
            this.secondActuator = secondActuator;
        }

        @Override
        public void actuate(Movable movable) {
            if(counter <= 10){
                this.firstActuator.actuate(movable);
            } else {
                this.secondActuator.actuate(movable);
                if(counter == 30){
                    counter = 0;
                }
            }
        }
    }
}
